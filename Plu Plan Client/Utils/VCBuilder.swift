//
//  VCBuilder.swift
//  We Plan
//
//  Created by apple on 9/24/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import UIKit

class VCBuilder {
  
    /**
       Returns an instance of LoginVC.
     - Returns: An instance of LoginVC.
     */
    class func homeVC() -> LoginVC {
      return LoginVC()
  }
  class func tittleVC() -> GetStartVC {
    return GetStartVC()
  }
}
