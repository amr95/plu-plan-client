//
//  UIView+Extension.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//
import UIKit
import NVActivityIndicatorView

extension UIView {
  
  /// EZSE: add multiple subviews
  public func addSubviews(_ views: [UIView]) {
    views.forEach { [weak self] eachView in
      self?.addSubview(eachView)
    }
  }
  
    func setGradientBackground(colorOne: UIColor, colorTwo: UIColor) {
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [colorOne.cgColor, colorTwo.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.0)
        
        layer.insertSublayer(gradientLayer, at: 0)
    }
  
}


//extension NVActivityIndicatorType {
//  static var random : NVActivityIndicatorType {
//    let randomTypes = [3,5,6,7,9,10,11,12,13,16,21,22,23,29,20]
//    let count = UInt32(randomTypes.count - 1)
//    NVActivityIndicatorType.init
//    return NVActivityIndicatorType.init(rawValue: randomTypes[Int(arc4random_uniform(count))])!
//  }
//}

extension UIView {
  @objc func showActivityIndicator(widthRatio: CGFloat = 80, shouldAdjustYAxis: Bool = false, color: UIColor = UIColor.white) {
    var ai = self.viewWithTag(1000) as? NVActivityIndicatorView
    if ai == nil {
      DispatchQueue.main.async {
        let frame = CGRect(x: 0, y: 0, width: widthRatio, height: widthRatio)
        ai = NVActivityIndicatorView(frame: frame, type: .ballRotateChase, color: #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1), padding: 10)
        ai?.tag = 1000
      }
    }
    DispatchQueue.main.async {
      ai?.center = CGPoint.init(x: self.bounds.midX, y: self.bounds.midY)
      if let button = self as? UIButton {
        button.setTitleColor(UIColor.clear, for: .normal)
        ai?.color = color
        self.superview?.insertSubview(ai!, aboveSubview: self)
      } else {
        if shouldAdjustYAxis {
          ai?.center.y -= 100
        }
        self.addSubview(ai!)
      }
      ai?.startAnimating()
    }
  }
  
  @objc func hideActivityIndicator(color: UIColor = UIColor.white) {
    DispatchQueue.main.async {
      if let ai = self.viewWithTag(1000) as? NVActivityIndicatorView {
        ai.stopAnimating()
        ai.removeFromSuperview()
      }
      if let button = self as? UIButton {
        button.setTitleColor(color, for: .normal)
      }
    }
  }
}


extension UIView {
  func shake() {
    let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    animation.duration = 0.8
    animation.values = [-20, 20, -20, 20, -10, 10, -5, 5, 0]
    layer.add(animation, forKey: "shake")
  }
  
  func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset =  offSet
    layer.shadowRadius = radius
    layer.shouldRasterize = true
    layer.rasterizationScale = scale ? UIScreen.main.scale : 1
  }
}


public extension UIWindow {
  public var visibleViewController: UIViewController? {
    return UIWindow.getVisibleViewControllerFrom(self.rootViewController)
  }
  
  public static func getVisibleViewControllerFrom(_ vc: UIViewController?) -> UIViewController? {
    if let nc = vc as? UINavigationController {
      return UIWindow.getVisibleViewControllerFrom(nc.visibleViewController)
    } else if let tc = vc as? UITabBarController {
      return UIWindow.getVisibleViewControllerFrom(tc.selectedViewController)
    } else {
      if let pvc = vc?.presentedViewController {
        return UIWindow.getVisibleViewControllerFrom(pvc)
      } else {
        return vc
      }
    }
  }
}


extension UIBarButtonItem {
  
  var view: UIView? {
    guard let view = self.value(forKey: "view") as? UIView else {
      return nil
    }
    return view
  }
  
  var frame: CGRect? {
    guard let view = self.value(forKey: "view") as? UIView else {
      return nil
    }
    return view.frame
  }
  
}
