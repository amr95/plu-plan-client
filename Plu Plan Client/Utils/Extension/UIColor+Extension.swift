//
//  UIColor+Extension.swift
//  We Plan
//
//  Created by apple on 9/23/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//
import UIKit

extension UIColor {
  enum WePaln {
    static let primaryColor = #colorLiteral(red: 0.9137254902, green: 0.2666666667, blue: 0.4156862745, alpha: 1)
  }
}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}
