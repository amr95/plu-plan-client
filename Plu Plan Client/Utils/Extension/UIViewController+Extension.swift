//
//  UIViewController+Extension.swift
//  We Plan
//
//  Created by apple on 9/24/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import EZSwiftExtensions

extension UIViewController {
  
  /**
   Show a toast with message.
   - Parameter message: The message to display.
   - Parameter duration: The duration time for displaying the toast.
   - Parameter completion: Completion handler.
   */
  func showToast(message: String, duration: TimeInterval = 2.0, completion: (() -> Void)? = nil) {
    let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
    DispatchQueue.main.async {
      self.present(alert, animated: true, completion: nil)
    }
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
      alert.dismiss(animated: true, completion: completion)
    }
  }
  
  /**
   Show a UIAlertController with a specified message.
   - Parameter title: The title.
   - Parameter message: The message.
   - Parameter completion: Completion handler.
   */
  func showAlert(title:String, message:String, completion:@escaping () -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let backView = alert.view.subviews.last?.subviews.last
    backView?.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
    backView?.layer.cornerRadius = 14
    let okAction = UIAlertAction(title: "OK", style: .cancel) { (_) in
      completion()
      //alert.dismiss(animated: true, completion: nil)
    }
    okAction.setValue(UIColor.WePaln.primaryColor, forKey: "titleTextColor")
    alert.addAction(okAction)
    present(alert, animated: true, completion: nil)
  }
  
  
  func segueToMainTabBar() {
    let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
    let nav = storyboard.instantiateViewController(withIdentifier: "MainTabBar")
    UIApplication.shared.keyWindow?.rootViewController = nav
  }
  
  func segueToLoginVC() {
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let nav = storyboard.instantiateViewController(withIdentifier: "Tittle")
    UIApplication.shared.keyWindow?.rootViewController = nav
  }
  
  func segueToGetStart() {
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let nav = storyboard.instantiateViewController(withIdentifier: "rootnav")
    UIApplication.shared.keyWindow?.rootViewController = nav
  }

  func setupImagesView(profile: UIImageView, cover: UIImageView) {
    profile.setImage(url: UserDefaults.standard.string(forKey: "profilePicture"))
    cover.setImage(url: UserDefaults.standard.string(forKey: "cover"))
  }
}
