//
//  UIImageView.swift
//  We Plan
//
//  Created by apple on 11/19/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import Kingfisher


extension UIImageView {
  func setImage(url:String?) {
    if url != nil {
      if let imageURL = URL(string: url!.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)!) {
        self.kf.setImage(with: imageURL, placeholder: UIImage(named: "default-placeholder"))
      } else {
        self.image = UIImage(named: "default-placeholder")
      }
    } else {
      self.image = UIImage(named: "default-placeholder")
    }
  }
}
