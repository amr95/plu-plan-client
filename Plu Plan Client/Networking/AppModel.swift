//
//  AppModel.swift
//  We Plan
//
//  Created by apple on 11/5/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

struct AppModel<T: Decodable>: Decodable {
  var status: statusEnum?
  var msg: String?
  var data: T?

  
  enum statusEnum: Int, Decodable {
    case fail = 405
    case success = 200
    case needsToBeActiviated = 403
    case tokenExpired = 300
  }
}
