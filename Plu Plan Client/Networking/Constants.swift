//
//  Constants.swift
//  We Plan
//
//  Created by apple on 11/5/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

struct Constants {
  static let baseURL = "http://we-plan.net/api/user/"
  static var jwt = ""
  static var category = "1"

}
