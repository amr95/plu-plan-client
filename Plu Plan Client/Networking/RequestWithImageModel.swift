//
//  RequestWithImageModel.swift
//  We Plan
//
//  Created by apple on 11/12/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

struct RequestWithImageModel {
  //let image: UIImage?
  let params: [String:Any]?
  //let fileName: String?
  let images: [String:UIImage]?
  
}

