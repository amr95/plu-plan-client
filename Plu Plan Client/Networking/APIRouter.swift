//
//  APIRouter.swift
//  We Plan
//
//  Created by apple on 11/4/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import Alamofire
import Localize_Swift


enum APIRouter: URLRequestConvertible {
  
  // MARK: - Auth
  //  case register(name: String, email: String, password: String, notification_token: Int, device_id: Int, category_id: Int, tools: [String])
  case register(model: RequestWithImageModel)
  
  case login(email: String, password: String)
  
  case codeConfirmation(email: String, code: Int)
  
  case forgetPassword(email: String)
  
  case changePassword(email: String, password: String, password_confirmation: String, code: Int)
  
  case cities(country: Int)
  
  case countries
  
  case categories
  
  // MARK: - profile
  // want to check birthdate type again
  case updateProfile(requestModel: RequestWithImageModel)
  //  case updateProfile(name: String, email: String, phone: String, birthDate: String, gender: String, cityID: Int, countryID: Int)
  
  case updatePassword(oldPassword: String, newPassword: String, newPasswordConform: String)
  
  case getProfile
  
  case getAlbums
  
  case getAlbumImages(albumID: Int)
  
  case createAlbum(albumName: String)
  
  //  case addImagesToAlbum(albumID: Int, images: UIImage)
  case addImagesToAlbum(requestModel: RequestWithImageModel)
  
  case editImage(requestModel: RequestWithImageModel)
  
  case deleteImage(imageId: Int)
  
  case deleteAlbum(albumId: Int)
  
  case getPackages
  
  case addPackage(requestModel: RequestWithImageModel)
  
  case editPackage(requestModel: RequestWithImageModel)
  
  case deletePackage(packageId: Int)
  
  case contact(name: String, phone: String, message: String)
  
  case getOrders
  
  // MARK: - Http Methoda
  
  var method: HTTPMethod {
    switch self {
    case .register, .login, .codeConfirmation, .forgetPassword, .changePassword, .deleteAlbum, .deleteImage, .editImage,
         .addImagesToAlbum, .createAlbum, .updateProfile, .updatePassword, .addPackage, .editPackage, .deletePackage, .contact:
      return .post
      
    case .cities, .countries, .categories, .getProfile, .getAlbums, .getAlbumImages, .getPackages, .getOrders :
      return .get
    }
  }
  
  
  // MARK: - parameters
  
  var parameters: Parameters? {
    
    switch self {
      
      //    case .register(let name, let email, let password, let notification_token, let device_id, let category_id, let tools):
      //      return ["name": name, "email": email, "password": password, "notification_token": notification_token, "device_id": device_id,
      //              "category_id": category_id, "tools": tools]
      
    case .login(let email, let password):
      return ["email": email, "password": password]
      
    case .codeConfirmation(let email, let code):
      return ["email": email, "code": code]
      
    case .forgetPassword(let email):
      return ["email": email]
      
    case .changePassword(let email, let password, let password_confirmation, let code):
      return ["email": email, "password": password, "password_confirmation": password_confirmation, "code":code]
      
    case .cities(let country):
      return ["country": country]
      
      
      //    case .updateProfile(let name, let email, let phone, let birthDate, let gender, let cityID, let countryID):
      //      return ["name":name,"email":email, "phone": phone, "birth_date": birthDate, "gender": gender,"city_id": cityID, "country_id": countryID ]
      
    case .updatePassword(let oldPassword, let newPassword, let newPasswordConform):
      return ["old_password": oldPassword, "new_password": newPassword, "new_password_confirmation": newPasswordConform]
      
    case .getAlbumImages(let albumID):
      return ["album_id": albumID]
      
    case .createAlbum(let albumName):
      return ["name": albumName]
      
      //    case .addImagesToAlbum(let albumID, let images):
      //      return ["album_id": albumID, "images": images]
      
    case .addImagesToAlbum(let albumID):
      return ["album_id": albumID]
      
    case .editImage(let imageID):
      return ["image_id": imageID]
      
    case .deleteImage(let imageId):
      return ["image_id": imageId]
      
    case .deleteAlbum(let albumId):
      return ["album_id": albumId]
      
    case .deletePackage(let packageId):
      return ["package_id": packageId]
      
    case . contact(let name, let phone, let message):
      return ["name": name,"phone":phone,"message":message]
    default:
      return nil
    }
  }
  
  // MARK: - Paths
  
  var path: String {
    
    switch self {
      
    case .register:
      return "auth/register"
    case .login:
      return "auth/login"
    case .codeConfirmation:
      return "auth/code-confirmation"
    case .forgetPassword:
      return "auth/forget-password"
    case .changePassword:
      return "auth/change-password"
    case .cities:
      return "auth/cities"
    case .countries:
      return "auth/countries"
    case .categories:
      return "categories"
    case .updateProfile:
      return "profile/update-profile"
    case .updatePassword:
      return "profile/update-password"
    case .getProfile:
      return "profile/get-profile"
    case .getAlbums:
      return "albums/get-albums"
    case .getAlbumImages:
      return "albums/get-images"
    case .createAlbum:
      return "albums/create"
    case .addImagesToAlbum:
      return "albums/add-images"
    case .editImage:
      return "albums/edit-image"
    case .deleteImage:
      return "albums/delete-image"
    case .deleteAlbum:
      return "albums/delete-album"
    case .getPackages:
      return "packages/get"
    case .addPackage:
      return "packages/add"
    case .editPackage:
      return "packages/edit"
    case .deletePackage:
      return "packages/delete"
    case .contact:
      return "contact"
    case .getOrders:
      return "orders/get"
    }
  }
  
  // MARK: - Parameter Encoding
  
  var encoding: ParameterEncoding {
    switch self {
    case .cities, .getAlbumImages, .createAlbum:
      return URLEncoding.queryString
    default:
      return URLEncoding.default
    }
  }
  
  
  var headers: HTTPHeaders? {
    switch self {
      
    case .register:
      return ["lang": Localize.currentLanguage()]
      
    case .login:
      return ["lang":Localize.currentLanguage() , "category":Constants.category]
      
    case .codeConfirmation:
      return ["lang": Localize.currentLanguage()]
      
    case .forgetPassword:
      return ["lang": Localize.currentLanguage()]
      
    case .changePassword:
      return ["lang": Localize.currentLanguage()]
      
    case .cities:
      return ["lang": Localize.currentLanguage()]
      
    case .countries:
      return ["lang": Localize.currentLanguage()]
      
    case .categories:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")!]
      
    case .updateProfile:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! ,"lang":Localize.currentLanguage() , "category":Constants.category]
      
    case .updatePassword:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! ,"lang":Localize.currentLanguage() , "category":Constants.category]
      
    case .getProfile:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! ,"lang":Localize.currentLanguage() , "category":Constants.category]
      
    case .getAlbums:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .getAlbumImages:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .createAlbum:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .addImagesToAlbum:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .editImage:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .deleteImage:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .deleteAlbum:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .getPackages:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .addPackage:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .editPackage:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
    case .deletePackage:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! , "category":Constants.category]
      
    case .contact:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! ,"lang":Localize.currentLanguage() , "category":Constants.category]
    case .getOrders:
      return ["jwt":UserDefaults.standard.string(forKey: "jwt")! ,"lang":Localize.currentLanguage() , "category":Constants.category]
    }
  }
  
  
  
  func asURLRequest() throws -> URLRequest {
    let url = try Constants.baseURL.asURL().appendingPathComponent(path)
    print("url = \(url)")
    var request = URLRequest(url: url)
    request.httpMethod = method.rawValue
    
    if let headers = headers {
      for (headerField, headerValue) in headers {
        request.setValue(headerValue, forHTTPHeaderField: headerField)
      }
    }
    
    if let parameters = parameters {
      return try encoding.encode(request, with: parameters)
    }
    
    return request
    
  }
}
