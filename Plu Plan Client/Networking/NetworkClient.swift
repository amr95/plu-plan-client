//
//  NetworkClient.swift
//  We Plan
//
//  Created by apple on 11/5/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import Alamofire

class NetworkClient {
  
  typealias onSuccess<T> = ( (T) -> ())
  typealias onSuccess2<T> = ( (Bool) -> ())

  typealias onFailure = ((_ error: String) -> ())
  typealias onFailure2 = ((_ error: Error) -> ())
  
  init() {}
  
  static func logout() {
    SaveUser.removeUserToDefaults()
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let nav = storyboard.instantiateViewController(withIdentifier: "rootnav")
    UIApplication.shared.keyWindow?.rootViewController = nav
  }
  
  static func performRequest<T>(_ object: T.Type, router: APIRouter, success: @escaping onSuccess<T>, failure: @escaping onFailure2) where T:Decodable {
    Alamofire.request(router).responseJSON { (response) in
      // ALAMOFIRE ERROR CHECK
      //            response.response?.statusCode
      if let error = response.error{
        //  failure(error.localizedDescription)
        return
      }
      // PROCESS..
      do {
        let data = try JSONDecoder().decode(AppModel<T>.self, from: response.data!)
        if(data.status == .fail){
          print("Error = \(String(describing: data.msg))")
          //         failure(data.msg ?? "please try again")
          return
        } else if data.status == .tokenExpired {
          NetworkClient.logout()
        } else {
          if let datax = data.data {
            success(datax)
          }
        }
      } catch let error{
        print(error)
      }
    }
  }
  
  
  static func performRequest2<T>(_ object: T.Type, router: APIRouter, model: RequestWithImageModel?, success: @escaping onSuccess<T>, failure: @escaping onFailure) where T:Decodable {
    
    if let model = model {
      
      Alamofire.upload(multipartFormData: { (mfd: MultipartFormData) in
        
        if let params = model.params {
          
          for (key, value) in params {
            print("\(key) = \(value)")
            mfd.append(String(describing: value).data(using: .utf8)!, withName: key)
          }
        }
        
        
        if let images = model.images {
          for (key, value) in images {
            print("\(key) = \(value)")
          }
          for image in images {
            if let imageData = image.value.jpegData(compressionQuality: 0.25) {
              mfd.append(imageData, withName: image.key, fileName: "images.jpeg", mimeType: "image/jpeg")
            }
          }
        }

        
      }, with: router){ (result: SessionManager.MultipartFormDataEncodingResult) in
        switch result {
          
        case .success(let request, _, _):
          request.responseJSON(completionHandler: { (response) in
            do {
              print(response)
              
              let data = try JSONDecoder().decode(AppModel<T>.self, from: response.data!)
              if(data.status == .fail) || (data.status == .needsToBeActiviated){
                print("Error = \(String(describing: data.msg))")
                failure(data.msg ?? "please try again")
                return
              } else {
                if let datax = data.data {
                  print("Upload Success: \(datax)")
                  success(datax)
                }
              }
            } catch let error{
              print(error)
              failure(error.localizedDescription)
            }
          })
        case .failure(_):
          print("Upload failled")
        }
        
      }
    }
      
    else {
      Alamofire.request(router).responseJSON { (response) in
        // ALAMOFIRE ERROR CHECK
        //            response.response?.statusCode
        if let error = response.error{
          failure(error.localizedDescription)
          return
        }
        // PROCESS..
        do {
          let data = try JSONDecoder().decode(AppModel<T>.self, from: response.data!)
          if(data.status == .fail){
            print("Error = \(String(describing: data.msg))")
            failure(data.msg ?? "please try again")
            return
          } else if data.status == .tokenExpired {
            NetworkClient.logout()
          }
          else {
            if let datax = data.data {
              success(datax)
            }
          }
        } catch let error{
          failure(error.localizedDescription)
        }
      }
    }
    
  }
  
  static func performRequest3<T>(_ object: T.Type, router: APIRouter, success: @escaping onSuccess2<Any>, failure: @escaping onFailure) where T:Decodable {
    Alamofire.request(router).responseJSON { (response) in
      // ALAMOFIRE ERROR CHECK
      //            response.response?.statusCode
      if let error = response.error{
        //  failure(error.localizedDescription)
        return
      }
      // PROCESS..
      do {
        let data = try JSONDecoder().decode(AppModel<T>.self, from: response.data!)
        if(data.status == .fail){
          print("Error = \(String(describing: data.msg))")
          failure(data.msg ?? "please try again")
          return
        } else if data.status == .tokenExpired {
          NetworkClient.logout()
        } else {
          success(true)
        }
      } catch let error{
        print(error)
      }
    }
  }
  
}


