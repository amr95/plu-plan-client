//
//  OrdersModel.swift
//  We Plan
//
//  Created by apple on 12/29/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

// MARK: - DataClass
struct OrdersModel: Codable {
  let currentPage: Int?
  let data: [SingleOrderDetails]?
  let firstPageURL: String?
  let from, lastPage: Int?
  let lastPageURL: String?
  let nextPageURL: String?
  let path: String?
  let perPage: Int?
  let prevPageURL: String?
  let to, total: Int?
  
  enum CodingKeys: String, CodingKey {
    case currentPage = "current_page"
    case data
    case firstPageURL = "first_page_url"
    case from
    case lastPage = "last_page"
    case lastPageURL = "last_page_url"
    case nextPageURL = "next_page_url"
    case path
    case perPage = "per_page"
    case prevPageURL = "prev_page_url"
    case to, total
  }
}

// MARK: - Datum
struct SingleOrderDetails: Codable {
  let id: Int?
  let title, price, packageDescription, status: String?
  let cancelationReason: String?
  let customerID: Int?
  let customerName, customerCover, customerProfilePricture, customerSocialImg: String?
  let rate: Int?
  let packagePicture: String?
  let date, orderDescription, location, phone: String?
  let addressDetails: String?
  
  enum CodingKeys: String, CodingKey {
    case id, title, price
    case packageDescription = "package_description"
    case status
    case cancelationReason = "cancelation_reason"
    case customerID = "customer_id"
    case customerName = "customer_name"
    case customerCover = "customer_cover"
    case customerProfilePricture = "customer_profile_pricture"
    case customerSocialImg = "customer_social_img"
    case rate
    case packagePicture = "package_picture"
    case date
    case orderDescription = "order_description"
    case location, phone
    case addressDetails = "address_details"
  }
}



