//
//  getAlbumsModel.swift
//  We Plan
//
//  Created by apple on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation


// MARK: - getAlbumsModel

struct getAlbumsModel: Codable {
  let currentPage: Int?
  let data: [AlbumDetails]?
  let firstPageURL: String?
  let from, lastPage: Int?
  let lastPageURL: String?
  let nextPageURL: String?
  let path: String?
  let perPage: Int?
  let prevPageURL: String?
  let to, total: Int?
  
  enum CodingKeys: String, CodingKey {
    case currentPage = "current_page"
    case data
    case firstPageURL = "first_page_url"
    case from
    case lastPage = "last_page"
    case lastPageURL = "last_page_url"
    case nextPageURL = "next_page_url"
    case path
    case perPage = "per_page"
    case prevPageURL = "prev_page_url"
    case to, total
  }
}

// MARK: - Datum
struct AlbumDetails: Codable {
  let id: Int?
  let name: String?
  let img: String?

}




