//
//  ProfileModel.swift
//  We Plan
//
//  Created by apple on 12/1/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

// MARK: - SingleUserDataModel

struct SingleUserDataModel: Codable {
  let id: Int?
  let name, email, cover, profilePricture: String?
  let phone, city, country, birthDate: String?
  let gender, jwt, socialToken, socialImg: String?
  let cityID, countryID, rate: Int?

  enum CodingKeys: String, CodingKey {
    case id, name, email, cover
    case profilePricture = "profile_pricture"
    case phone, city, country
    case birthDate = "birth_date"
    case gender, jwt
    case socialToken = "social_token"
    case socialImg = "social_img"
    case cityID = "city_id"
    case countryID = "country_id"
    case rate
  }
}
