//
//  LoginModel.swift
//  We Plan
//
//  Created by Amr Saleh on 10/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

// MARK: - LoginModel
struct LoginModel: Decodable {
    let id: Int?
    let name, email, cover, profilePricture: String?
    let phone, city, country, birthDate: String?
    let gender, jwt, socialToken, socialImg: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, cover
        case profilePricture = "profile_pricture"
        case phone, city, country
        case birthDate = "birth_date"
        case gender, jwt
        case socialToken = "social_token"
        case socialImg = "social_img"
    }
}
