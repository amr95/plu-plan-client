//
//  PackageModel.swift
//  We Plan
//
//  Created by apple on 11/20/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//


import Foundation

// MARK: - News
struct packageModel: Codable {
  let status: Int?
  let msg: String?
  let data: SinglePackage?
}

// MARK: - DataClass
struct SinglePackage: Codable {
  let currentPage: Int?
  let data: [PackageData]?
  let firstPageURL: String?
  let from, lastPage: Int?
  let lastPageURL: String?
  let nextPageURL: String?
  let path: String?
  let perPage: Int?
  let prevPageURL: String?
  let to, total: Int?
  
  enum CodingKeys: String, CodingKey {
    case currentPage = "current_page"
    case data
    case firstPageURL = "first_page_url"
    case from
    case lastPage = "last_page"
    case lastPageURL = "last_page_url"
    case nextPageURL = "next_page_url"
    case path
    case perPage = "per_page"
    case prevPageURL = "prev_page_url"
    case to, total
  }
}

// MARK: - Datum
struct PackageData: Codable {
  let id: Int?
  let title, price: String?
  let image: String?
  let datumDescription: String?
  
  enum CodingKeys: String, CodingKey {
    case id, title, price, image
    case datumDescription = "description"
  }
}

