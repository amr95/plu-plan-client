//
//  CountryModel.swift
//  We Plan
//
//  Created by apple on 11/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation


// MARK: - SingleCountryData
struct SingleCountryData: Decodable {
  let id: Int?
  let name: String?
}
