//
//  CategoriesModel.swift
//  Plu Plan Client
//
//  Created by apple on 2/25/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//


struct CategoriesModel: Codable {
  let id: Int?
  let name: String?
  let img: String?
}
