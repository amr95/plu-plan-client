//
//  GetStartVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import MOLH

class GetStartVC: UIViewController {

    @IBOutlet weak var getStartButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        getStartButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
    
    @IBAction func getStartButtonTapped(_ sender: UIButton) {
      if SaveUser.isUserAvailabelOnDefaults {
        print("jwt = \(UserDefaults.standard.string(forKey: "jwt")!)")
        self.segueToMainTabBar()
      } else {
//        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
//        MOLH.reset(transition: .transitionCrossDissolve)
        self.segueToLoginVC()
      }
    }
    
    

}
