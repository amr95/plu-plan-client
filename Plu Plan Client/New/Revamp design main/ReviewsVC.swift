//
//  ReviewsVC.swift
//  We Plan
//
//  Created by apple on 11/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class ReviewsVC: UIViewController, IndicatorInfoProvider {
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return IndicatorInfo(title: "Reviews".localized())
  }
  

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
