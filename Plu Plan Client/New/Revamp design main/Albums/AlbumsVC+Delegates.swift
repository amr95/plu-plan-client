//
//  AlbumsVC+Delegates.swift
//  We Plan
//
//  Created by apple on 11/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

extension AlbumsVC: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return albums.count + 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.row == 0 {
      let cell = collectionView.dequeue(indexPath: indexPath) as AddNewAlbumCVC
      cell.plusIV.layer.borderWidth = 2
      cell.plusIV.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
      return cell
    } else {
      let cell = collectionView.dequeue(indexPath: indexPath) as AlbumCVC
      
      if self.albums.count > 0 {
        if let imageURL = self.albums[indexPath.row - 1].img {
          if !imageURL.isEmpty {
            cell.albumImage.kf.indicatorType = .activity
            cell.albumImage.kf.setImage(with: URL(string: imageURL) )
            
          } else {
            cell.albumImage.image = UIImage(named: "x1")
          }
        }
      }
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row == 0 {
      let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
      let nav = storyboard.instantiateViewController(withIdentifier: "AddNewAlbum")
      UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
    } else {
      let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
      let nav = storyboard.instantiateViewController(withIdentifier: "ViewAlbum") as! ViewAlbumImagesVC
      guard let albumID = albums[indexPath.row - 1].id else {return}
      guard let albumName = albums[indexPath.row - 1].name else {return}
      nav.albumID = albumID
      nav.albumName = albumName
      print(albumID)
      UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
}

extension AlbumsVC : UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: 150, height: 150)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 15.0, left: 30.0, bottom: 0.0, right: 30.0)
  }
  
}
