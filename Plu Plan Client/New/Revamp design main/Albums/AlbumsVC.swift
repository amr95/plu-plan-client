//
//  AlbumsVC.swift
//  We Plan
//
//  Created by apple on 11/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class AlbumsVC: UIViewController, IndicatorInfoProvider {
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return IndicatorInfo(title: "Portfolio".localized())
  }
  
  
  var albumID = 0
  
  var albums = [AlbumDetails]() {
    didSet {
      collectionView.reloadData()
    }
  }
  
  let presenter = AlbumsPresenter()
  

  @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
      presenter.attatchView(view: self)
      presenter.startNetworking()
      
      
      collectionView.dataSource = self
      collectionView.delegate = self
        // Do any additional setup after loading the view.
      collectionView.registerCellNib(cellClass: AlbumCVC.self)
      collectionView.registerCellNib(cellClass: AddNewAlbumCVC.self)
      
      // MARK: - add long press to collectionview
      let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGestureRecognizer:)))
      collectionView.addGestureRecognizer(longPress)


    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    presenter.startNetworking()
  }
  
  
  // MARK: - Handle long press on collectionview cell
  @objc func handleLongPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
    
    if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
      let touchPoint = longPressGestureRecognizer.location(in: self.collectionView)
      if let indexPath = self.collectionView.indexPathForItem(at: touchPoint) {
        // add your code here
        // you can use 'indexPath' to find out which row is selected
          if (indexPath.row - 1) >= 0 {
            if let albumID = albums[indexPath.row - 1].id {
              self.albumID = albumID
              self.showLongpressAlert(indexPath: indexPath)
            }
          }
      }
    }
  }
  
  // MARK: - Delete Alert
  fileprivate func deleteAlert(title: String, message: String, indexPath: IndexPath) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let yesDelete = UIAlertAction(title: "Yes sure", style: .destructive) { (action: UIAlertAction) in
        self.presenter.deleteAlbum(albumID: self.albumID)
    }
    
    let noThanks = UIAlertAction(title: "No Thanks", style: .cancel)
    
    alert.addAction(yesDelete)
    alert.addAction(noThanks)
    
    self.presentVC(alert)
  }
  
  fileprivate func showLongpressAlert(indexPath: IndexPath) {
    let alert = UIAlertController(title: "", message: "What do you want ?", preferredStyle: .actionSheet)
    let editAction = UIAlertAction(title: "Edit", style: .default) { (action: UIAlertAction) in
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "ViewAlbum") as! ViewAlbumImagesVC
        guard let albumID = self.albums[indexPath.row - 1].id else {return}
        guard let albumName = self.albums[indexPath.row - 1].name else {return}
        nav.albumID = albumID
        nav.albumName = albumName
        print(albumID)
        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
    }
    
    let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
      
      self.deleteAlert(title: "Delete Album", message: "Are you sure you want to delete this Album !!", indexPath: indexPath)

    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }

}




extension AlbumsVC: AlbumsViewDelegate {
 
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
    
  }
  
  func refesh() {
    self.presenter.startNetworking()
  }
  
  func displayError(error: String) {
    
  }
  
  func displayAlbumData(with album: [AlbumDetails]) {
    self.albums = album
  }
  
  
}
