//
//  AddNewAlbumVC.swift
//  We Plan
//
//  Created by Amr Saleh on 11/9/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class AddNewAlbumVC: UIViewController {

    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var albumNameTextField: UITextField!
    
    let presenter = CreateAlbumPresenter()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.attatchView(view: self)
    }
    
    override func viewDidLayoutSubviews() {
        createButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }

    @IBAction func closeButtonPresed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAlbumDidPresed(_ sender: UIButton) {
        guard let albumName = albumNameTextField.text else {return}
        presenter.startNetworking(albumName: albumName)
    }
    
    
    
}

extension AddNewAlbumVC: CreateAlbumDelegate {
    func displayLoading() {
        self.view.showActivityIndicator()
    }
    
    func dismissLoading() {
        self.view.hideActivityIndicator()
    }
    
    func displayError() {
        
    }
    
    func displayError(error: String) {
        self.showToast(message: error)
    }
    
    func segueToMain() {
      self.dismiss(animated: true, completion: nil)
    }
    
    
}
