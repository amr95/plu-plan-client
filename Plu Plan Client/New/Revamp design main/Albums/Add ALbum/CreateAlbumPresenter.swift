//
//  CreateAlbumPresenter.swift
//  We Plan
//
//  Created by Amr Saleh on 11/16/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol CreateAlbumDelegate {
    func displayLoading()
    func dismissLoading()
    func displayError()
    func displayError(error: String)
    func segueToMain()
}

class CreateAlbumPresenter {
    
    var delegate: CreateAlbumDelegate?
    
    // attatch view to presenter
    func attatchView(view: CreateAlbumDelegate){
        delegate = view
    }
    // detach view to presenter
    func detachView(){
        delegate = nil
    }
    
    func startNetworking(albumName: String) {
        self.delegate?.displayLoading()
        
        NetworkClient.performRequest2(getAlbumsModel.self, router: .createAlbum(albumName: albumName), model: nil, success: { (response) in
            self.delegate?.dismissLoading()
            print("success")
            self.delegate?.segueToMain()
        }) { (error) in
            self.delegate?.dismissLoading()
            self.delegate?.displayError(error: error)

            print(error)
        }
        
    }
    
    
    
    
}


