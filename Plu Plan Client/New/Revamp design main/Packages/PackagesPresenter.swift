//
//  PackagesPresenter.swift
//  We Plan
//
//  Created by apple on 11/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol PackagesViewDelegate {
  func displayLoading()
  func dismissLoading()
  func refesh()
  func displayError(error: String)
  func displayData(with packages: [PackageData])
  
}

class PackagesPresenter {
  
  var delegate: PackagesViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: PackagesViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  
  func fetchPackagesData () {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest2(SinglePackage.self, router: .getPackages, model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      
      if let data = response.data {
        self.delegate?.displayData(with: data)
      }
      
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
    
  }
  
  
  
  
  func deleteAlbum(albumID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deleteAlbum(albumId: albumID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.refesh()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
  
  func deletePackage(packageID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deletePackage(packageId: packageID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.refesh()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
}
