//
//  PackagesVC+Delegates.swift
//  We Plan
//
//  Created by apple on 11/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

extension PackagesVC: UICollectionViewDataSource, UICollectionViewDelegate {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return packages.count + 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    if indexPath.row == 0 {
      let cell = collectionView.dequeue(indexPath: indexPath) as AddNewPackageCVC
      
      return cell
    } else {
      let cell = collectionView.dequeue(indexPath: indexPath) as PackagesCVC
      cell.configureCell(packagesData: packages[indexPath.row - 1])
      return cell
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    if indexPath.row == 0 {
      let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
      let nav = storyboard.instantiateViewController(withIdentifier: "AddNewPackage")
      UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
    } else {
      // view package details
      let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
      let nav = storyboard.instantiateViewController(withIdentifier: "ViewPackageDetails") as! ViewPackageDetailsVC
      guard let name = packages[indexPath.row - 1].title else { return}
      guard let image = packages[indexPath.row - 1].image else { return}
      guard let desc = packages[indexPath.row - 1].datumDescription else { return}
      guard let price = packages[indexPath.row - 1].price else { return}
      
      nav.desc = desc
      nav.image = image
      nav.name = name
      nav.price = price
      UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
    }
  }
  
}

extension PackagesVC : UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: UIScreen.main.bounds.width, height: 110.0)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
  }
  
}
