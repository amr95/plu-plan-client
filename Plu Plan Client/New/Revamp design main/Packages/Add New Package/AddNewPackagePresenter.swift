//
//  AddNewPackagePresenter.swift
//  We Plan
//
//  Created by apple on 11/21/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

protocol CreatePackageDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError()
  func displayError(error: String)
  func dismiss()
}

class AddNewPackagePresenter {
  var delegate: CreatePackageDelegate?
  
  // attatch view to presenter
  func attatchView(view: CreatePackageDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func createPackage(description: String, title: String, price: String, image: UIImage) {
    self.delegate?.displayLoading()
    
    let params = ["title":title, "price":price, "description":description]
    
    let model = RequestWithImageModel(params: params, images: ["image" : image])
    
    NetworkClient.performRequest2(SinglePackage.self, router: .addPackage(requestModel: model), model: model, success: { (response) in
      
      self.delegate?.dismissLoading()
      print("success")
      self.delegate?.dismiss()
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
      
      print(error)
    }
    
  }
  
  func editPackage(description: String, title: String, price: String, image: UIImage, packageId: Int) {
    self.delegate?.displayLoading()
    
    let params = ["title":title, "price":price, "description":description, "package_id":packageId] as [String : Any]
    
    let model = RequestWithImageModel(params: params, images: ["image" : image])

    NetworkClient.performRequest2(SinglePackage.self, router: .editPackage(requestModel: model), model: model, success: { (response) in
      
      self.delegate?.dismissLoading()
      print("success")
      self.delegate?.dismiss()
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
      
      print(error)
    }
    
  }
}
