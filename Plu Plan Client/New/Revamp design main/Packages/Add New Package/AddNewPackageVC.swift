//
//  AddNewPackageVC.swift
//  We Plan
//
//  Created by Amr Saleh on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class AddNewPackageVC: UIViewController {

  @IBOutlet weak var newButton: UIButton!
  @IBOutlet weak var packageNameTextfield: UITextField!
  @IBOutlet weak var packagePriceTextField: UITextField!
  @IBOutlet weak var packageDesc: UITextField!
  @IBOutlet weak var packageImage: UIImageView!
  
  let presenter = AddNewPackagePresenter()
  let imagePicker = UIImagePickerController()

  var pickedImage: UIImage? {
    didSet {
      // execute some code
      DispatchQueue.main.async {
        self.packageImage.image = self.pickedImage

      }
    }
  }
  
  var name = ""
  var desc = ""
  var price = ""
  var imageUrl = ""
  var buttonTittle = ""
  var packageID = 0
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleImageTapped(sender:)))
        self.packageImage.addGestureRecognizer(tapGesture)
    
        presenter.attatchView(view: self)
        imagePicker.delegate = self
    
        packagePriceTextField.text = price
        packageDesc.text = desc
        packageNameTextfield.text = name
        if !imageUrl.isEmpty {
          packageImage.setImage(url: imageUrl)
        }
        newButton.setTitle(buttonTittle, for: .normal)
    }
  
  
    override func viewDidLayoutSubviews() {
        newButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
  
  
  // MARK: - choose photo source to upload image
  fileprivate func photoSourceActionSheet() {
    let alert = UIAlertController(title: "", message: "Photo source", preferredStyle: .actionSheet)
    
    let editAction = UIAlertAction(title: "Photo Library", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .photoLibrary
      self.presentVC(self.imagePicker)
    }
    
    let deleteAction = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .camera
      self.presentVC(self.imagePicker)
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
    }
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }
  
  
  
  @IBAction func closeButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  
  @IBAction func createButton(_ sender: UIButton) {
    guard let name = packageNameTextfield.text else {
      self.showToast(message: "please enter package name")
      return
    }
    
    guard let price = packagePriceTextField.text else {
      self.showToast(message: "please enter package price")
      return
    }
    
    guard let desc = packageDesc.text else {
      self.showToast(message: "please enter package description")
      return
    }
    
    guard let image = packageImage.image else {
      self.showToast(message: "please enter package name")
      return
    }
    if newButton.title(for: .normal) == "update package" {
      presenter.editPackage(description: desc, title: name, price: price, image: image, packageId: packageID )
    } else {
      presenter.createPackage(description: desc, title: name, price: price, image: image)
    }
  }
  


  @objc func handleImageTapped(sender: UITapGestureRecognizer) {
    photoSourceActionSheet()
  }
  
  
}

extension AddNewPackageVC: CreatePackageDelegate {
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError() {
    
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func dismiss() {
    self.dismiss(animated: true, completion: nil)
  }
  
}


// MARK: - extension image picker controller

extension AddNewPackageVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    imagePicker.dismiss(animated: true, completion: nil)
  }
  
 @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
        self.pickedImage = editedImage
      }
      
     else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
        self.pickedImage = originalImage
      }
    
    imagePicker.dismiss(animated: true, completion: nil)
    
  }
  
}
