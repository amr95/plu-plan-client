//
//  ViewPackageDetailsVC.swift
//  We Plan
//
//  Created by apple on 11/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class ViewPackageDetailsVC: UIViewController {

  @IBOutlet weak var packageImageView: UIImageView!
  @IBOutlet weak var packageNameTextField: UITextField!
  @IBOutlet weak var packageDesc: UITextField!
  @IBOutlet weak var packagePriceTextField: UITextField!
  
  var name = ""
  var price = ""
  var desc = ""
  var image = ""
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        packageImageView.setImage(url: image)
        packageNameTextField.text = name
        packagePriceTextField.text = price
        packageDesc.text = desc
    }
    

//  func setup() {
//    guard let image = packageData?.image else { return}
//    guard let name = packageData?.title else { return}
//    guard let price = packageData?.price else { return}
//    guard let desc = packageData?.datumDescription else { return}
//
//   // packageImageView.setImage(url: image)
//    packageNameTextField.text = name
//    packagePriceTextField.text = price
//    packageDesc.text = desc
//  }
  @IBAction func close(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
}
