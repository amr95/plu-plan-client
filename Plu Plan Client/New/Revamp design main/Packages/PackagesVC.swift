//
//  PackagesVC.swift
//  We Plan
//
//  Created by apple on 11/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class PackagesVC: UIViewController, IndicatorInfoProvider {
  func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
    return IndicatorInfo(title: "Packages".localized())
  }
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var packages = [PackageData]()
  {
    didSet {
      collectionView.reloadData()
    }
  }
  
  let presenter = PackagesPresenter()

  
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      collectionView.registerCellNib(cellClass: PackagesCVC.self)
      collectionView.registerCellNib(cellClass: AddNewPackageCVC.self)
      
      collectionView.dataSource = self
      collectionView.delegate = self
      
      presenter.attatchView(view: self)
      // MARK: - add long press to collectionview
      let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGestureRecognizer:)))
      collectionView.addGestureRecognizer(longPress)
      
      presenter.fetchPackagesData()
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    presenter.fetchPackagesData()
  }

  // MARK: - Handle long press on collectionview cell
  @objc func handleLongPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
    
    if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
      let touchPoint = longPressGestureRecognizer.location(in: self.collectionView)
      if let indexPath = self.collectionView.indexPathForItem(at: touchPoint) {
        // add your code here
        // you can use 'indexPath' to find out which row is selected
          if (indexPath.row - 1) >= 0 {
            if let packageID = packages[indexPath.row - 1].id {
              self.showLongpressAlert(indexPath: indexPath)
              print("package id = \(packageID)")
            
          }
        }
      }
    }
  }
  
  
  fileprivate func showLongpressAlert(indexPath: IndexPath) {
    let alert = UIAlertController(title: "", message: "What do you want ?", preferredStyle: .actionSheet)
    let editAction = UIAlertAction(title: "Edit", style: .default) { (action: UIAlertAction) in

        
        
        
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "AddNewPackage") as! AddNewPackageVC
        nav.buttonTittle = "update package"
        nav.name = self.packages[indexPath.row - 1].title!
        nav.desc = self.packages[indexPath.row - 1].datumDescription!
        nav.price = self.packages[indexPath.row - 1].price!
        nav.imageUrl = self.packages[indexPath.row-1].image!
        nav.packageID = self.packages[indexPath.row-1].id!
        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
        
    
    }
    
    let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in

        self.deleteAlert(title: "Delete Package", message: "Are you sure you want to delete this Package !!", indexPath: indexPath)
      
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
    }
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }
  
  // MARK: - Delete Alert
  fileprivate func deleteAlert(title: String, message: String, indexPath: IndexPath) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let yesDelete = UIAlertAction(title: "Yes sure", style: .destructive) { (action: UIAlertAction) in
        print("hello from delete")
        self.presenter.deletePackage(packageID: self.packages[indexPath.row-1].id!)
    }
    
    let noThanks = UIAlertAction(title: "No Thanks", style: .cancel)
    
    alert.addAction(yesDelete)
    alert.addAction(noThanks)
    
    self.presentVC(alert)
  }
  
  
  
}

extension PackagesVC: PackagesViewDelegate {
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func refesh() {
    presenter.fetchPackagesData()
  }
  
  func displayError(error: String) {
    showToast(message: error)
  }
  
  func displayData(with packages: [PackageData]) {
    self.packages = packages
  }
  
  
}
