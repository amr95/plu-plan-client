//
//  MainVC.swift
//  We Plan
//
//  Created by apple on 11/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class MainVC: ButtonBarPagerTabStripViewController {
  @IBOutlet weak var profileImage: UIImageView!
    override func viewDidLoad() {
      settings.style.buttonBarBackgroundColor = .white
      settings.style.buttonBarItemBackgroundColor = .white
      settings.style.selectedBarBackgroundColor = #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1)
      settings.style.buttonBarItemFont = .boldSystemFont(ofSize: 16)
      settings.style.selectedBarHeight = 2.0
      settings.style.buttonBarMinimumLineSpacing = 0
      settings.style.buttonBarItemTitleColor = .black
      settings.style.buttonBarItemsShouldFillAvailableWidth = true
      settings.style.buttonBarLeftContentInset = 0
      settings.style.buttonBarRightContentInset = 0
      
      changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
        guard changeCurrentIndex == true else { return }
        oldCell?.label.textColor = .black
        newCell?.label.textColor = #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1)
      }
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
  override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
    let child_1 = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "child1")
    let child_2 = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "child2")
    let child_3 = UIStoryboard(name: "Profile", bundle: nil).instantiateViewController(withIdentifier: "child3")
    
    return [child_1, child_2, child_3]
    
  }
}
