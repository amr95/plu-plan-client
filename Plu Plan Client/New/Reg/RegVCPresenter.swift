//
//  RegVCPresenter.swift
//  We Plan
//
//  Created by apple on 12/3/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

protocol RegisterViewDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError()
  func displayError(error: String)
  func segueToHome()
}

class RegisterPresenter {
  
  var delegate: RegisterViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: RegisterViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  //    case .register(let name, let email, let password, let notification_token, let device_id, let category_id, let tools):
  //      return ["name": name, "email": email, "password": password, "notification_token": notification_token, "device_id": device_id,
  //              "category_id": category_id, "tools": tools]

  func registerUser(name: String, email: String, password: String, categoryID: Int, tool1: UIImage?, tool2: UIImage?, tool3:UIImage?) {
    self.delegate?.displayLoading()
    
    let params = ["name": name, "email": email, "password": password,
                  "category_id": categoryID, "notification_token":0000, "device_id":0000 ] as [String : Any]
    
    var images = [String: UIImage]()
    
    if let tool1 = tool1 {
      images["tools[0]"] = tool1
    }
    
    if let tool2 = tool2 {
      images["tools[1]"] = tool2
    }
    
    if let tool3 = tool3 {
      images["tools[2]"] = tool3
    }
    
    let model = RequestWithImageModel(params: params, images: images)
    
    NetworkClient.performRequest2(SingleUserDataModel.self, router: .register(model: model), model: model, success: { (response) in
      print(response)
      self.delegate?.dismissLoading()
      self.delegate?.segueToHome()
    }) { (error) in
      print(error)
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
  
  
}
