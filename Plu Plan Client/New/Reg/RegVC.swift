//
//  RegVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/9/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class RegVC: UIViewController {

  @IBOutlet weak var continueButton: UIButton!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var phoneTextField: UITextField!
  @IBOutlet weak var fullNameTextField: UITextField!
  @IBOutlet weak var tool1ImageView: UIImageView!
  @IBOutlet weak var tool2ImageView: UIImageView!
  @IBOutlet weak var tool3ImageView: UIImageView!
  
  let imagePicker = UIImagePickerController()
  let presenter = RegisterPresenter()
  
  var isTool1 = false
  var isTool2 = false
  
  var categoryID = 0
  
  var tool1Image: UIImage? {
    didSet {
      // execute some code
      guard let image = tool1Image else { return }
      // send to server
      tool1ImageView.image = image
    }
  }
  
  var tool2Image: UIImage? {
    didSet {
      // execute some code
      guard let image = tool2Image else { return }
      // send to server
      tool2ImageView.image = image
    }
  }
  
  var tool3Image: UIImage? {
    didSet {
      // execute some code
      guard let image = tool3Image else { return }
      // send to server
      tool3ImageView.image = image
    }
  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Registration"
        continueButton.layer.cornerRadius = continueButton.frame.height / 2
        continueButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
        imagePicker.delegate = self
        presenter.attatchView(view: self)
    }

    
    override func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @IBAction func continueButtonTapped(_ sender: UIButton) {
      presenter.registerUser(name: fullNameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, categoryID: categoryID, tool1: tool1Image, tool2: tool2Image, tool3: tool3Image)
  }
    
  @IBAction func selectTitleButtonTapped(_ sender: UIButton) {
    TitleTableViewController.PopupPresenter().showPopup(sender: sender) { (title,categoryId)  in
      sender.setTitleColor(.black, for: .normal)
      sender.setTitle(title, for: .normal)
      self.categoryID = categoryId
    }
  }

  @IBAction func tool1DidTapped(_ sender: Any) {
    self.photoSourceActionSheet()
    isTool1 = true
  }
  
  @IBAction func tool2DidTapped(_ sender: Any) {
    self.photoSourceActionSheet()
    isTool2 = true
    isTool1 = false
  }
  @IBAction func tool3DidTapped(_ sender: Any) {
    isTool2 = false
    isTool1 = false
    self.photoSourceActionSheet()
   
  }
  
  // MARK: - choose photo source to upload image
  fileprivate func photoSourceActionSheet() {
    let alert = UIAlertController(title: "", message: "Photo source", preferredStyle: .actionSheet)
    
    let editAction = UIAlertAction(title: "Photo Library", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .photoLibrary
      self.presentVC(self.imagePicker)
    }
    
    let deleteAction = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .camera
      self.presentVC(self.imagePicker)
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }
  
}

extension RegVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    imagePicker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {

      if isTool1 {
        self.tool1Image = editedImage
      } else if isTool2 {
        self.tool2Image = editedImage
      } else {
        self.tool3Image = editedImage
      }
      
    } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
      if isTool1 {
        self.tool1Image = originalImage
      } else if isTool2 {
        self.tool2Image = originalImage
      } else {
        self.tool3Image = originalImage
      }
    }
    imagePicker.dismiss(animated: true, completion: nil)
    
  }
  
}

extension RegVC: RegisterViewDelegate {
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError() {
    
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func segueToHome() {
    self.segueToLoginVC()
  }
  
  
}
