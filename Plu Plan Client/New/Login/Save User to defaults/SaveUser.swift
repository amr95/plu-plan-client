//
//  SaveUser.swift
//  We Plan
//
//  Created by apple on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

class SaveUser {
  
  static func saveUserToDefaults(user: LoginModel) {
    UserDefaults.standard.set(user.id, forKey: "id")
    UserDefaults.standard.set(user.email, forKey: "email")
    UserDefaults.standard.set(user.name, forKey: "name")
    UserDefaults.standard.set(user.phone, forKey: "phone")
    UserDefaults.standard.set(user.city, forKey: "city")
    UserDefaults.standard.set(user.country, forKey: "country")
    UserDefaults.standard.set(user.birthDate, forKey: "birthDate")
    UserDefaults.standard.set(user.gender, forKey: "gender")
    UserDefaults.standard.set(user.jwt, forKey: "jwt")
    UserDefaults.standard.set(user.cover, forKey: "cover")
    UserDefaults.standard.set(user.profilePricture, forKey: "profilePicture")
  }
  
  static func removeUserToDefaults() {
    UserDefaults.standard.removeObject(forKey: "id")
    UserDefaults.standard.removeObject(forKey: "email")
    UserDefaults.standard.removeObject(forKey: "name")
    UserDefaults.standard.removeObject(forKey: "phone")
    UserDefaults.standard.removeObject(forKey: "city")
    UserDefaults.standard.removeObject(forKey: "country")
    UserDefaults.standard.removeObject(forKey: "birthDate")
    UserDefaults.standard.removeObject(forKey: "gender")
    UserDefaults.standard.removeObject(forKey: "jwt")
    UserDefaults.standard.removeObject(forKey: "cover")
    UserDefaults.standard.removeObject(forKey: "profilePicture")
    UserDefaults.standard.synchronize()
  }

//  private func loadUserFromDefaults() {
//    self.id = UserDefaults.standard.string(forKey: "id") ?? ""
//    self.name = UserDefaults.standard.string(forKey: "name") ?? ""
//    self.mail = UserDefaults.standard.string(forKey: "mail") ?? ""
//    self.mobile = UserDefaults.standard.string(forKey: "mobile") ?? ""
//    self.image = UserDefaults.standard.string(forKey: "image") ?? ""
//    UserDefaults.standard.synchronize()
//  }

  
  static var isUserAvailabelOnDefaults : Bool {
   if let id = UserDefaults.standard.string(forKey: "id"),
      let name = UserDefaults.standard.string(forKey: "name"),
      let mail = UserDefaults.standard.string(forKey: "email")
       {
      return !id.isEmpty && !name.isEmpty && !mail.isEmpty
    }
    return false
  }

}
