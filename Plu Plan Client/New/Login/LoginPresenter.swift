//
//  LoginPresenter.swift
//  We Plan
//
//  Created by Amr Saleh on 10/27/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol LoginViewDelegate {
    func displayLoading()
    func dismissLoading()
    func displayError()
    func displayError(error: String)
    func segueToMain()
}

class LoginPresenter {
    
    var delegate: LoginViewDelegate?
    
    // attatch view to presenter
    func attatchView(view: LoginViewDelegate){
        delegate = view
    }
    // detach view to presenter
    func detachView(){
        delegate = nil
    }
    
    func startNetworking(email: String , password: String) {
        self.delegate?.displayLoading()

      NetworkClient.performRequest2(LoginModel.self, router: .login(email: email, password: password), model: nil, success: { (response) in
                self.delegate?.dismissLoading()
                let data = response
                print("Welcome \(String(describing: data.name))")
                print("JWT = \(data.jwt)")
                SaveUser.saveUserToDefaults(user: data)
                self.delegate?.segueToMain()
      }) { (error) in
                self.delegate?.dismissLoading()
        self.delegate?.displayError(error: error)
                print("test error =\(error)")
      }
      
    }
    
  
    
    
}
