//
//  ContactPresenter.swift
//  We Plan
//
//  Created by apple on 12/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol CotactViewDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError(error: String)
  func segueToMain()
}

class ContactPresenter {
  var delegate: CotactViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: CotactViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func sendMessage(name: String, message: String, phone: String){
    delegate?.displayLoading()
    NetworkClient.performRequest3(MessageModel.self, router: .contact(name: name, phone: phone, message: message), success: { (response) in
      self.delegate?.dismissLoading()
      self.delegate?.segueToMain()
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
}
