//
//  ContactusVC.swift
//  We Plan
//
//  Created by apple on 12/26/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class ContactusVC: UIViewController, UITextViewDelegate {

  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var messageTextView: UITextView!
  @IBOutlet weak var phoneTextField: UITextField!
  @IBOutlet weak var sendMessageButton: UIButton!
  
  let presenter = ContactPresenter()
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    presenter.attatchView(view: self)
    
    messageTextView.delegate = self
    self.messageTextView.text = "Message"
    self.messageTextView.textColor = UIColor.lightGray
    }
 
  
  override func viewDidLayoutSubviews() {
    sendMessageButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
  }

  func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
    if text == "\n" {
      textView.resignFirstResponder()
      return false
    }
    return true
  }
  
  // MARK: - TextField Deleagtes Method
  func textViewDidBeginEditing(_ textView: UITextView) {
    if textView.textColor == UIColor.lightGray{
      textView.text = nil
      textView.textColor = UIColor.black
    }
    textView.becomeFirstResponder()
  }
  
  func textViewDidEndEditing(_ textView: UITextView) {
    if textView.text.isEmpty {
      DispatchQueue.main.async {
        textView.text = "Message"
        textView.textColor = UIColor.lightGray
      }
    }
  }
  
  @IBAction func closeButton(_ sender: Any) {
    self.dismiss(animated: true)
  }
  @IBAction func sendMessageButtonTapped(_ sender: Any) {
    
    if let name = nameTextField.text,
      let phone = phoneTextField.text,
      let message = messageTextView.text {
      presenter.sendMessage(name: name, message: message, phone: phone)
    }
    
  }
  
  
}

extension ContactusVC: CotactViewDelegate {
  func segueToMain() {
    self.dismiss(animated: true)
  }
  
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
}
