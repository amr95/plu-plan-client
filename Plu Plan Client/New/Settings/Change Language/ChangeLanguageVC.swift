//
//  ChangeLanguageVC.swift
//  We Plan
//
//  Created by apple on 12/25/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import MOLH

class ChangeLanguageVC: UIViewController {

  @IBOutlet weak var arabicButton: UIButton!
  @IBOutlet weak var englishButton: UIButton!
  
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  
  override func viewDidLayoutSubviews() {
    arabicButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    englishButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
  }
    
  @IBAction func closeButtonTapped(_ sender: Any) {
    self.dismiss(animated: true)
  }
  @IBAction func arabicButtonTapped(_ sender: Any) {
    if MOLHLanguage.currentAppleLanguage() == "en" {
      MOLH.setLanguageTo("ar")
      MOLH.reset(transition: .transitionCrossDissolve)
    } else {
      self.showToast(message: "lang")
    }
    //        MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
    //        MOLH.reset(transition: .transitionCrossDissolve)
  }
  
  @IBAction func englishButtonTapped(_ sender: Any) {
    if MOLHLanguage.currentAppleLanguage() == "ar" {
      MOLH.setLanguageTo("en")
      MOLH.reset(transition: .transitionCrossDissolve)
    } else {
      self.showToast(message: "lang")
    }
  }
}
