//
//  ChangePasswordPresenter .swift
//  We Plan
//
//  Created by apple on 12/25/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
protocol ChangePasswordViewDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError(error: String)
  func segueToLogin()
}
class ChangePasswordPresenter {
  var delegate: ChangePasswordViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: ChangePasswordViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func changePassword(email: String,password: String , passwordConfirmation: String, code: Int) {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest2(LoginModel.self, router: .changePassword(email: email, password: password, password_confirmation: passwordConfirmation, code: code), model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      SaveUser.removeUserToDefaults()
      self.delegate?.segueToLogin()
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
  
  
}
