//
//  CheckingEmailVC.swift
//  We Plan
//
//  Created by apple on 12/25/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class CheckingEmailVC: UIViewController {

  @IBOutlet weak var emailTextField: UITextField!
  
  @IBOutlet weak var enterButton: UIButton!
  
  var email = ""
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "gotoChangePassword" {
      let target = segue.destination as! ChangePasswordVC
      target.email = self.email
    }
  }
  override func viewDidLayoutSubviews() {
    enterButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
  }

  @IBAction func closeButtonTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func enterButtonTapped(_ sender: Any) {
    if let email = emailTextField.text {
      if email == UserDefaults.standard.string(forKey: "email") {
        self.email = email
        self.performSegue(withIdentifier: "gotoChangePassword", sender: nil)
      }else {
        showToast(message: "please enter your email")
      }
    } else {
      showToast(message: "please enter your email")
    }
  }
  
}
