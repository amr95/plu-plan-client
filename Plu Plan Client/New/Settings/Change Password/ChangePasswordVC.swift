//
//  ChangePasswordVC.swift
//  We Plan
//
//  Created by Amr Saleh on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {

  @IBOutlet weak var oldPasswordTextField: UITextField!
  @IBOutlet weak var newPasswordTextField: UITextField!
  @IBOutlet weak var changePasswordButton: UIButton!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var codeTextField: UITextField!
  
  var email = ""
  let presenter = ChangePasswordPresenter()
  
  override func viewDidLoad() {
        super.viewDidLoad()
    presenter.attatchView(view: self)
        // Do any additional setup after loading the view.
    if !email.isEmpty {
      emailTextField.text = email
    }
    
    }
    
    override func viewDidLayoutSubviews() {
      changePasswordButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
  
  @IBAction func closeButtonTapped(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)

  }
  @IBAction func changePasswordButtonTapped(_ sender: Any) {
    if let password = oldPasswordTextField.text,
       let confirmPassword = newPasswordTextField.text,
      let code = Int(codeTextField.text!) {
      presenter.changePassword(email: self.email, password: password, passwordConfirmation: confirmPassword, code: code)
    }
  }
}

extension ChangePasswordVC: ChangePasswordViewDelegate {
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func segueToLogin() {
    self.segueToLogin()
  }
  
  
}
