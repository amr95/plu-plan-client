//
//  SettingsTableViewController.swift
//  We Plan
//
//  Created by apple on 12/8/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath == IndexPath(row: 0, section: self.tableView.numberOfSections - 1) {
      DispatchQueue.main.async {
        self.logout()
      }
    }
  }
  
  func logout() {
    let alert = UIAlertController(title: NSLocalizedString("Are you sure you want to logout ?", comment: ""), message: nil, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .destructive, handler: {
      action in
      SaveUser.removeUserToDefaults()
      self.segueToGetStart()
    }))
    
    alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil))
    
    self.present(alert, animated: true, completion: nil)
  }

}
