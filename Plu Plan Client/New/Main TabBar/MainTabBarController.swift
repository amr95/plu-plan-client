//
//  MainTabBarController.swift
//  We Plan
//
//  Created by Amr Saleh on 11/9/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController , UITabBarControllerDelegate  {
    
    deinit {
        print("deinit: MainTabBarController")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      self.tabBarController?.selectedIndex = 2

        // Do any additional setup after loading the view.
        self.delegate = self
        self.tabBar.itemPositioning = .fill
        let itemSize = CGSize(width: tabBar.frame.width / 5, height: tabBar.frame.height - 1)
        let renderer = UIGraphicsImageRenderer(size: itemSize)
        self.tabBar.selectionIndicatorImage = renderer.image { (ctx) in
            ctx.cgContext.setFillColor(UIColor.white.withAlphaComponent(0.1).cgColor)
            ctx.fill(CGRect(origin: CGPoint.zero, size: itemSize))
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setupAppearance()
        self.tabBarController?.selectedIndex = 2

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    var didClickOnce = false
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if didClickOnce {
            if let nav = viewController as? UINavigationController {
                if let tvc = nav.viewControllers.first! as? UITableViewController {
                    tvc.tableView.setContentOffset(CGPoint.zero, animated: true)
                }
            }
        }
        
        self.didClickOnce = true
        Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false, block: { _ in
            self.didClickOnce = false
        })
    }
    
    
    func setupAppearance() {
        UINavigationBar.appearance().prefersLargeTitles = true
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barStyle = .black
//        UINavigationBar.appearance().tintColor = #colorLiteral(red: 0.9960784314, green: 0.8705882353, blue: 0, alpha: 1)
//        UINavigationBar.appearance().barTintColor = #colorLiteral(red: 0.1725490196, green: 0.1725490196, blue: 0.1725490196, alpha: 1)
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.init(name: "GE SS", size: 22)!]
//        UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.font: UIFont.init(name: "GESSTextBold-Bold", size: 30)!]
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = #colorLiteral(red: 0.9137254902, green: 0.2666666667, blue: 0.4156862745, alpha: 1)
        UITabBar.appearance().barTintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        UITabBar.appearance().unselectedItemTintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        
        let size = CGSize.init(width: UIApplication.shared.keyWindow?.frame.size.width ?? 0, height: 1.5)
        let renderer = UIGraphicsImageRenderer(size: size)
        let shadowImage = renderer.image { (ctx) in
            ctx.cgContext.setFillColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor)
            ctx.fill(CGRect(origin: CGPoint.zero, size: size))
        }
        
        UINavigationBar.appearance().shadowImage = shadowImage
        UITabBar.appearance().shadowImage = shadowImage
        UITabBar.appearance().backgroundImage = UIImage()
    }
    
//    func setupGlobalAppearance(){
//        let customFont = APIUser.isEnglish ? UIFont.systemFont(ofSize: 17) : UIFont.init(name: "GE SS", size: 17)!
//        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: customFont], for: .normal)
//        UITextField.appearance().substituteFontName = customFont.fontName
//        UILabel.appearance().substituteFontName = customFont.fontName
//    }
}




//    
//class MainTabBar: UITabBar, UITabBarDelegate {
//        
//        var midButton = UIButton()
//        
//        lazy var mainTabBarController: MainTabBarController = {
//            let mtb = UIApplication.shared.keyWindow?.visibleViewController?.tabBarController
//            return mtb as! MainTabBarController
//        }()
//        
//        override func awakeFromNib() {
//            super.awakeFromNib()
//            self.setupMiddleButton()
//        }
//        
//        override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//            if self.isHidden {
//                return super.hitTest(point, with: event)
//            }
//            
//            return self.midButton.frame.contains(point) ? self.midButton : super.hitTest(point, with: event)
//        }
//        
//        func setupMiddleButton() {
//            DispatchQueue.main.async {
//                self.midButton = UIButton(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
//                self.midButton.layer.cornerRadius = self.midButton.bounds.height / 2
//                self.midButton.center.x = self.center.x
//                self.midButton.center.y = self.frame.origin.x + 25
//                self.midButton.tintColor = .white
//                self.midButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
//                self.midButton.setImage(#imageLiteral(resourceName: "ic_plus"), for: .normal)
//                self.addSubview(self.midButton)
//                self.midButton.addTarget(self, action: #selector(self.segueToAddNewNavigationController(sender:)), for: .touchUpInside)
//            }
//        }
//    
//    @objc func segueToAddNewNavigationController(sender: UIButton) {
// 
//        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
//        let nav = storyboard.instantiateViewController(withIdentifier: "MainScreen") 
//        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
//    }
//    
//    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
//        return .none
//    }
//}
