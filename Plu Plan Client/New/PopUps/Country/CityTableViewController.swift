//
//  CityTableViewController.swift
//  We Plan
//
//  Created by apple on 12/1/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import Localize_Swift

class CityTableViewController: UITableViewController {
  typealias CityCompletion = ( _ city: String, _ id: Int) -> ()
  
  
  var tableHeight:CGFloat = 400.0
  var city = [SingleCountryData]()
  static var countryID = 0
  
  var completion: CityCompletion!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.tableView.separatorStyle = .none
    self.tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    print("cID = \(CityTableViewController.countryID)")
    reloadData()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  func reloadData() {
    self.view.showActivityIndicator()
    
    NetworkClient.performRequest2([SingleCountryData].self, router: .cities(country: CityTableViewController.countryID), model: nil, success: { (response) in
      print(response)
      
      self.city = response
      self.tableView.reloadData()
      
      if self.city.count < 10 {
        self.tableHeight = (45 * CGFloat(self.city.count))
      }
      DispatchQueue.main.async {
        self.preferredContentSize = CGSize(width: self.preferredContentSize.width, height: self.tableHeight)
      }
      
      self.view.hideActivityIndicator()
      
      print(self.city.count)
    }) { (error) in
      self.view.hideActivityIndicator()
      self.showToast(message: error)
    }
  }
  
  // MARK: - Table view data source
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    // #warning Incomplete implementation, return the number of sections
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    // #warning Incomplete implementation, return the number of rows
    return city.count
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 45
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
    
    if cell == nil {
      cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
    }
    
    cell?.textLabel?.text = self.city[indexPath.row].name
    cell?.textLabel?.adjustsFontSizeToFitWidth = true
    cell?.textLabel?.textAlignment = .center
    cell?.backgroundColor = UIColor.clear
    cell?.textLabel?.textColor = #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1)
    
    return cell!
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
    self.completion(city[indexPath.row].name!, city[indexPath.row].id!)
    self.dismiss(animated: true, completion: nil)
  }
  
  class PopupPresenter: NSObject, UIPopoverPresentationControllerDelegate {
    func showPopup(sender: UIButton, completion: @escaping CityCompletion) {
      let fromVC = UIApplication.shared.keyWindow!.visibleViewController!
      let dvc = CityTableViewController.init(style: .plain)
      
      dvc.completion = completion
      
      dvc.modalPresentationStyle = .popover
      dvc.preferredContentSize = CGSize(width: sender.bounds.width, height: 90)
      
      let popover = dvc.popoverPresentationController!
      popover.delegate = self
      popover.sourceView = sender
      popover.sourceRect = CGRect(x: sender.bounds.midX, y: sender.bounds.midY , width: 0, height: 0)
      popover.permittedArrowDirections = Localize.currentLanguage() == "en" ? [.left]:[.right]

      fromVC.present(dvc, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
      return .none
    }
  }
  
  deinit {
    print("deinit: TableViewController")
  }
  
  
  
}
