//
//  countryVC.swift
//  We Plan
//
//  Created by apple on 11/28/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class countryVC: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  typealias CountryCompletion = ( _ country: String, _ id: Int) -> ()

  var countries = [SingleCountryData]()
  var completion: CountryCompletion?

  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    tableView.dataSource = self
    tableView.delegate = self
    }
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    reloadData()
  }
  
  func reloadData() {
    self.view.showActivityIndicator()
    NetworkClient.performRequest2([SingleCountryData].self, router: .countries, model: nil, success: { (response) in
      print(response)

      self.countries = response
      self.tableView.reloadData()
      self.view.hideActivityIndicator()

      print(self.countries.count)
    }) { (error) in
      self.view.hideActivityIndicator()
      self.showToast(message: error)
    }
  }
  
  deinit {
    print("deinit: CountryTableViewController")
  }

}

extension countryVC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return countries.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
    
    if cell == nil {
      cell = UITableViewCell.init(style: .default, reuseIdentifier: "Cell")
    }
    cell?.textLabel?.text = self.countries[indexPath.row].name
    cell?.textLabel?.adjustsFontSizeToFitWidth = true
    cell?.textLabel?.textAlignment = .center
    cell?.backgroundColor = UIColor.clear
    cell?.textLabel?.textColor = #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1)
    return cell!
  }
  
   func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    self.tableView.deselectRow(at: indexPath, animated: true)
    
    if let name = countries[indexPath.row].name, let id = countries[indexPath.row].id {
      self.completion?(name,id)
    }
    
    self.dismiss(animated: true, completion: nil)
  }
  
  
  class PopupPresenter: NSObject, UIPopoverPresentationControllerDelegate {
    func showPopup(sender: UIButton, completion: @escaping CountryCompletion) {
      let fromVC = UIApplication.shared.keyWindow!.visibleViewController!
      let dvc = countryVC()
      
      dvc.completion = completion
      
      dvc.modalPresentationStyle = .popover
      dvc.preferredContentSize = CGSize(width: sender.bounds.width, height: 90)
      
      let popover = dvc.popoverPresentationController!
      popover.delegate = self
      popover.sourceView = sender
      popover.sourceRect = CGRect(x: sender.bounds.midX, y: sender.bounds.midY , width: 0, height: 0)
      popover.permittedArrowDirections = [.left, .right]
      
      fromVC.present(dvc, animated: true, completion: nil)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
      return .none
    }
  }
  
}
