//
//  OrderDetailsVC.swift
//  Plu Plan Client
//
//  Created by apple on 2/27/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//

import UIKit

class OrderDetailsVC: UIViewController {

  @IBOutlet weak var tableView: UITableView!
  
  var order: SingleOrderDetails?
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      tableView.registerCellNib(cellClass: OrderDetailsTableViewCell.self)
      tableView.delegate = self
      tableView.dataSource = self
    }


}

extension OrderDetailsVC: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue() as OrderDetailsTableViewCell
    if let orderDetails = self.order {
      cell.configurCell(order: orderDetails)
    }
    return cell
  }
  
  
}
