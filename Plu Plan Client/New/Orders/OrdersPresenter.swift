//
//  OrdersPresenter.swift
//  We Plan
//
//  Created by apple on 12/29/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol OrdersViewDelegate {
  func displayLoading()
  func dismissLoading()
  func refesh()
  func displayError(error: String)
  func displayOrderData(with order: [SingleOrderDetails])
}

class OrdersPresenter {
  
  var delegate: OrdersViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: OrdersViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func fetchOrdersData() {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest2(OrdersModel.self, router: .getOrders, model: nil, success: { (response) in
      self.delegate?.dismissLoading()

      self.processData(with: response as AnyObject)

    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
  
  func processData(with Model: AnyObject) {
    let orders = Model as! OrdersModel
    if let ordersArray = orders.data {
      self.sendDataToView(with: ordersArray)
    } else {
      return
    }
  }
  
  //checking the data count and if ispaginating or not to send it to the view
  func sendDataToView(with data : [SingleOrderDetails]) {
    
    //    DispatchQueue.main.async {
    //      if data.count == 0 {
    //        if !self.isPaginating {
    //          self.movieView?.displayError()
    //        }
    //        return
    //      }
    //      if !self.isPaginating {
    //        self.movieView?.displayData(with: data)
    //      }
    //      else {
    //        self.movieView?.displayPagintationData(with: data)
    //      }
    //    }
    self.delegate?.displayOrderData(with: data)
  }
  
}
