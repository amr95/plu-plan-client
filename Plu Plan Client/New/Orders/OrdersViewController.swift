//
//  OrdersViewController.swift
//  We Plan
//
//  Created by apple on 12/29/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class OrdersViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  var orders = [SingleOrderDetails]() {
    didSet{
      tableView.reloadData()
    }
  }
  
  let presenter = OrdersPresenter()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    tableView.registerCellNib(cellClass: OrdersCell.self)
    tableView.delegate = self
    tableView.dataSource = self
    presenter.attatchView(view: self)
   // presenter.fetchOrdersData()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    presenter.fetchOrdersData()
  }
  
}

extension OrdersViewController: OrdersViewDelegate {
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func refesh() {
    
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func displayOrderData(with order: [SingleOrderDetails]) {
    self.orders = order
  }  
}
