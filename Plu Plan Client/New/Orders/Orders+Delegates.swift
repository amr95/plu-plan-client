//
//  Orders+Delegates.swift
//  We Plan
//
//  Created by apple on 12/29/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

extension OrdersViewController: UITableViewDelegate, UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.orders.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeue() as OrdersCell
    cell.configurCell(order: self.orders[indexPath.row])
    cell.delegate = self
    return cell
  }

  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 240
  }
}

extension OrdersViewController: OrdersCellDelegate {
  func viewOrderDetails(orderDetails: SingleOrderDetails) {
//    let vc = OrderDetailsVC()
//    vc.order = orderDetails
//    self.present(vc, animated: true)
    let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
    let nav = storyboard.instantiateViewController(withIdentifier: "orderDetails") as! OrderDetailsVC
    nav.order = orderDetails
    UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
  }
}
