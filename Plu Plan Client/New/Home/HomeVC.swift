//
//  HomeVC.swift
//  Plu Plan Client
//
//  Created by apple on 2/25/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//

import UIKit

class HomeVC: UIViewController {

  @IBOutlet weak var collectionView: UICollectionView!
  
  var categories = [CategoriesModel]()
  let presenter = HomePresenter()
  
  override func viewDidLoad() {
        super.viewDidLoad()
    self.collectionView.delegate = self
    self.collectionView.dataSource = self
    collectionView.registerCellNib(cellClass: CategoriesTypeCollectionViewCell.self)
    presenter.attatchView(view: self)
    presenter.fetchCategoriesData()
    }
}

extension HomeVC: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return self.categories.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeue(indexPath: indexPath) as CategoriesTypeCollectionViewCell
    if !categories.isEmpty {
      cell.categoryImageView.setImage(url: categories[indexPath.row].img)
      cell.categoryNameLabel.text = categories[indexPath.row].name
    }
    cell.cellView.layer.cornerRadius = (collectionView.frame.width / 2 - 5) / 2
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
    let width = collectionView.frame.width / 2 - 5
    return CGSize(width: width, height: width)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    return 5.0
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
    return 5.0
  }
}

extension HomeVC: HomeVcDelegate {
  func displayData(with categoryData: [CategoriesModel]) {
    self.categories = categoryData
    self.collectionView.reloadData()
  }
  
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError() {
    
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }

  
}
