//
//  HomePresenter.swift
//  Plu Plan Client
//
//  Created by apple on 2/25/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//

import UIKit

protocol HomeVcDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError()
  func displayError(error: String)
  func displayData(with categoryData:[CategoriesModel])
}

class HomePresenter {
  var delegate: HomeVcDelegate?
  
  // attatch view to presenter
  func attatchView(view: HomeVcDelegate){
    delegate = view
  }
  
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func fetchCategoriesData() {
    self.delegate?.displayLoading()
    NetworkClient.performRequest2([CategoriesModel].self, router: .categories, model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      self.delegate?.displayData(with: response)
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
}
