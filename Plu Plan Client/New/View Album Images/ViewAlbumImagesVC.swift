//
//  ViewAlbumImagesVC.swift
//  We Plan
//
//  Created by apple on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import Kingfisher
import GSImageViewerController

class ViewAlbumImagesVC: UIViewController {

  @IBOutlet weak var albumNameLabel: UILabel!
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var albumID = 0
  var albumName = "Album Name"
  var imageID = 0
  var isEdited = false { didSet {print("edit = \(isEdited)")}}
  
  let presenter = GetAlbumImagesPresenter()
  let imagePicker = UIImagePickerController()
  
  var albumImages = [ImagesData]() {
    didSet {
      collectionView.reloadData()
    }
  }
  
  var pickedImage: UIImage? {
    didSet {
      // execute some code
      guard let image = pickedImage else { return }
      // send to server
        presenter.addImage(image: image, albumID: albumID)
    }
  }
  
  
  
  var editedImage: UIImage? {
    didSet{
      guard let image = editedImage else { return }

      presenter.editImage(image: image, imageID: imageID)
      isEdited = false
    }
  }
  
  
  
  
    override func viewDidLoad() {
        super.viewDidLoad()
      presenter.attatchView(view: self)
      presenter.startNetworking(albumID: albumID)
      albumNameLabel.text = albumName
      
      
      
        // Do any additional setup after loading the view.
      self.collectionView.delegate = self
      self.collectionView.dataSource = self
      collectionView.registerCellNib(cellClass: ImagesCVC.self)
      collectionView.registerCellNib(cellClass: AddCVC.self)
      
      imagePicker.delegate = self
    }
  
  @IBAction func closeButton(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func deleteButtonTapped(_ sender: Any) {
    self.deleteAlert(title: "Delete Album", message: "Are you sure you want to delete this album !!")
  }
  
  
  func reloadData() {
    presenter.startNetworking(albumID: albumID)
  }
  
  func deleteImage() {
    presenter.deleteImage(imageID: imageID)
  }
  
  // MARK: - choose photo source to upload image
  fileprivate func photoSourceActionSheet() {
    let alert = UIAlertController(title: "", message: "Photo source", preferredStyle: .actionSheet)
    
    let editAction = UIAlertAction(title: "Photo Library", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .photoLibrary
      self.presentVC(self.imagePicker)
    }
    
    let deleteAction = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .camera
      self.presentVC(self.imagePicker)
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
      self.isEdited = false
    }
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }
  
  // MARK: - Delete Alert
  fileprivate func deleteAlert(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    
    let yesDelete = UIAlertAction(title: "Yes sure", style: .default) { (action: UIAlertAction) in
      if title == "Delete Image" {
        self.deleteImage()
      } else {
        self.presenter.deleteAlbum(albumID: self.albumID)
      }
    }
    
    let noThanks = UIAlertAction(title: "No Thanks", style: .cancel) { (action: UIAlertAction) in
    }
    
    
    
    alert.addAction(yesDelete)
    alert.addAction(noThanks)
    
    self.presentVC(alert)
  }
  

  
}

// MARK: - extension collection view delegate and datasource

extension ViewAlbumImagesVC: UICollectionViewDelegate , UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return albumImages.count + 1
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    // MARK: - Add new photo cell
    if indexPath.row == 0 {
      let cell = collectionView.dequeue(indexPath: indexPath) as AddCVC
      cell.delegate = self
      cell.addImageView.layer.borderWidth = 2
      cell.addImageView.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
      return cell
    }
      // MARK: - album Images cell
    else {
      let cell = collectionView.dequeue(indexPath: indexPath) as ImagesCVC
      cell.delegate = self

      if let imageURL = self.albumImages[indexPath.row - 1].ur{
        if !imageURL.isEmpty {
          
          cell.configureCell(imageData: albumImages[indexPath.row-1])
          
        } else {
          cell.albumImage.image = UIImage(named: "x1")
        }
      }
      return cell
    }
    
  }
  
}


// MARK: - collection view item size

extension ViewAlbumImagesVC : UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      return CGSize(width: 150, height: 150)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
      return UIEdgeInsets(top: 15.0, left: 30.0, bottom: 0.0, right: 30.0)
  }
  
}


// MARK: - presenter delegates methods

extension ViewAlbumImagesVC: getAlbumImagesDelegate {
  func reloadCollectionView() {
    self.reloadData()
  }
  
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError() {
    self.dismiss(animated: true, completion: nil)
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func displayData(with imagesData: [ImagesData]) {
    self.albumImages = imagesData
  }
  
  
}

// MARK: - extension image did tapped

extension ViewAlbumImagesVC: LongPressDelegate {
 
  
  func didClickOnImage(image: UIImage, imageID: Int) {
    
    self.imageID = imageID
    print("imageID = \(imageID)")
    let imageInfo = GSImageInfo(image: image, imageMode: .aspectFit, imageHD: nil)
    let transitionInfo = GSTransitionInfo(fromView: self.view)
    let imageViewer = GSImageViewerController(imageInfo: imageInfo, transitionInfo: transitionInfo)
    present(imageViewer, animated: true, completion: nil)
  }
  
  
  func didPressedLong(imageID: Int) {
    self.imageID = imageID
    print("imageID = \(imageID)")
    let alert = UIAlertController(title: "LongPress", message: "LongPress", preferredStyle: .actionSheet)
    let editAction = UIAlertAction(title: "Edit", style: .default) { (action: UIAlertAction) in
      self.isEdited = true
      self.photoSourceActionSheet()
    }
    
    let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
      self.deleteAlert(title: "Delete Image", message: "Are you sure you want to delete this image !!")
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
      self.isEdited = false
    }
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)

    self.presentVC(alert)
    
    
  }
  
}

// MARK: - extension image picker controller

extension ViewAlbumImagesVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    isEdited = false
    imagePicker.dismiss(animated: true, completion: nil)
  }

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
      if isEdited {
        self.editedImage = editedImage
        self.isEdited = false
      } else {
        self.pickedImage = editedImage
      }
      
    } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
      if isEdited {
        self.editedImage = originalImage
      } else {
        self.pickedImage = originalImage
      }
    }
    imagePicker.dismiss(animated: true, completion: nil)

  }
  
}

// MARK: - extension add image did tapped

extension ViewAlbumImagesVC: addDidTappedDelegate {
  
  func addImageDidTapped() {
    
    photoSourceActionSheet()
  }
  
}
