//
//  getAlbumImagesPresenter.swift
//  We Plan
//
//  Created by apple on 11/17/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

protocol getAlbumImagesDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError()
  func displayError(error: String)
  func displayData(with imagesData:[ImagesData])
  func reloadCollectionView()
}

class GetAlbumImagesPresenter {
  
  var delegate: getAlbumImagesDelegate?
  
  // attatch view to presenter
  func attatchView(view: getAlbumImagesDelegate){
    delegate = view
  }
  
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func startNetworking(albumID: Int) {
    self.delegate?.displayLoading()
    NetworkClient.performRequest2(AlbumImages.self, router: .getAlbumImages(albumID: albumID), model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      
      self.processData(with: response as AnyObject)
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
  
  
  func processData(with Model: AnyObject) {
    let albumImages = Model as! AlbumImages
    if let albumsImages = albumImages.data {
      self.sendDataToView(with: albumsImages)
    } else {
      return
    }
  }
  
  func sendDataToView(with data : [ImagesData]) {
    self.delegate?.displayData(with: data)
    
  }
  
  func addImage(image: UIImage, albumID: Int) {
    delegate?.displayLoading()
    let params = ["album_id": albumID]
    let model = RequestWithImageModel(params: params, images: ["images[0]" : image])

    
    NetworkClient.performRequest2(AlbumImages.self, router: .addImagesToAlbum(requestModel: model), model: model , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.reloadCollectionView()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }

  }
  
  func editImage(image: UIImage, imageID: Int) {
    delegate?.displayLoading()
    let params = ["image_id": imageID]
    let model = RequestWithImageModel(params: params, images: ["image" : image])

    
    NetworkClient.performRequest2(AlbumImages.self, router: .editImage(requestModel: model), model: model , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.reloadCollectionView()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }

  func deleteImage(imageID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deleteImage(imageId: imageID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.reloadCollectionView()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
  
  func deleteAlbum(albumID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deleteAlbum(albumId: albumID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
}
