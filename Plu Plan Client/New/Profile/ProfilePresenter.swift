//
//  ProfilePresenter.swift
//  We Plan
//
//  Created by apple on 12/1/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit

protocol ProfileViewDelegate {
  func displayLoading()
  func dismissLoading()
  func displayError()
  func displayError(error: String)
  func displayUserData(user: SingleUserDataModel)
  func segueToHome()
}

class ProfilePresenter {
  
  var delegate: ProfileViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: ProfileViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func fetchUserData() {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest2(SingleUserDataModel.self, router: .getProfile, model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      let data = response
      self.delegate?.displayUserData(user: data)
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
    
  }


  func updateProfile(name: String, email: String, phone: String, birthDate: String, gender: String, cityID: Int, countryID: Int, profileImage: UIImage? , coverImage: UIImage?) {
    self.delegate?.displayLoading()

    let params = ["name":name,"email":email, "phone": phone, "birth_date": birthDate, "gender": gender,"city_id": cityID, "country_id": countryID ] as [String : Any]
    
    
        var images = [String: UIImage]()
    
        if let profile = profileImage {
          images["profile_picture"] = profile
        }
    
        if let cover = coverImage {
          images["cover"] = cover
        }

    let model = RequestWithImageModel(params: params, images: images)

    
    NetworkClient.performRequest2(SingleUserDataModel.self, router: .updateProfile(requestModel: model), model: model, success: { (response) in
      
      self.delegate?.dismissLoading()
      self.delegate?.displayUserData(user: response)
      self.delegate?.segueToHome()
    }) { (error) in
      print(error)
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
  }
}

