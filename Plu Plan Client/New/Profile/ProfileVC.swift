//
//  ProfileVC.swift
//  We Plan
//
//  Created by Amr Saleh on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var enterButton: UIButton!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var phoneTextField: UITextField!
  @IBOutlet weak var countryButton: UIButton!
  @IBOutlet weak var cityButton: UIButton!
  @IBOutlet weak var birthDateTextField: UITextField!
  @IBOutlet weak var genderButton: UIButton!
  
  @IBOutlet weak var mainProfileImageView: UIImageView!
  @IBOutlet weak var mainCoverImageView: UIImageView!
  
  @IBOutlet weak var coverImageView: UIImageView!
  @IBOutlet weak var profileImageView: UIImageView!
  
  @IBOutlet weak var nameView: UIView!
  @IBOutlet weak var emailView: UIView!
  @IBOutlet weak var phoneView: UIView!
  @IBOutlet weak var genderView: UIView!
  
  @IBOutlet weak var cityView: UIView!
  @IBOutlet weak var birthDateView: UIView!
  @IBOutlet weak var countryView: UIView!

  
  var name = ""
  var email = ""
  var birthDate = ""
  var gender = ""
  var phone = ""
  var countryID = 0
  var cityID = 0

  
  let presenter =  ProfilePresenter()
  
  let imagePicker = UIImagePickerController()
  let datePicker = UIDatePicker()
  
  var IsCoverPhoto = true
  var isCoverPhotoEdited = false
  var isProfilePhotoEdited = false

  var profileImage: UIImage? {
    didSet {
      // execute some code
      guard let image = profileImage else { return }
      // send to server
      profileImageView.image = image
      isProfilePhotoEdited = true
    }
  }
  
  var coverImage: UIImage? {
    didSet {
      // execute some code
      guard let image = coverImage else { return }
      // send to server
      coverImageView.image = image
      isCoverPhotoEdited = true
    }
  }
  
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    presenter.attatchView(view: self)
    imagePicker.delegate = self
    showDatePicker()
    self.setupImagesView(profile: mainProfileImageView, cover: mainCoverImageView)
    presenter.fetchUserData()
    }
  
    override func viewDidLayoutSubviews() {
        enterButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
//    presenter.fetchUserData()
  }

  
  // MARK: - choose photo source to upload image
  fileprivate func photoSourceActionSheet() {
    let alert = UIAlertController(title: "", message: "Photo source", preferredStyle: .actionSheet)
    
    let editAction = UIAlertAction(title: "Photo Library", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .photoLibrary
      self.presentVC(self.imagePicker)
    }
    
    let deleteAction = UIAlertAction(title: "Camera", style: .default) { (action: UIAlertAction) in
      self.imagePicker.sourceType = .camera
      self.presentVC(self.imagePicker)
    }
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) 
    
    alert.addAction(editAction)
    alert.addAction(deleteAction)
    alert.addAction(cancelAction)
    
    self.presentVC(alert)
  }
  
  @IBAction func countryButtonTapped(_ sender: UIButton) {
    countryID = 0
    sender.setTitle("Country".localized(), for: .normal)
    sender.setTitleColor(.gray, for: .normal)
    cityButton.setTitle("City".localized(), for: .normal)
    cityButton.setTitleColor(.gray, for: .normal)

    
    CountryTableViewController.PopupPresenter().showPopup(sender: sender) { (name: String, id: Int) in
      self.countryID = id
      sender.setTitleColor(.black, for: .normal)
      sender.setTitle(name, for: .normal)
    }
  }
  
  @IBAction func cityButtonTapped(_ sender: UIButton) {
    if countryID == 0 {
      self.countryView.shake()
    } else {
      CityTableViewController.countryID = self.countryID
      CityTableViewController.PopupPresenter().showPopup(sender: sender) { (name: String, id: Int) in
        self.cityID = id
        sender.setTitleColor(.black, for: .normal)
        sender.setTitle(name, for: .normal)
      }
    }
  }
  
  
  @IBAction func genderButtonTapped(_ sender: UIButton) {
    GenderTableViewController.PopupPresenter().showPopup(sender: sender) { (gender) in
      sender.setTitle(gender, for: .normal)
    }
  }
  
  @IBAction func updateProfileButton(_ sender: Any) {

    if let name = nameTextField.text {
      self.name = name
    }
    
    if let email = emailTextField.text {
      self.email = email
    }
    
    if let phone = phoneTextField.text {
      self.phone = phone
    }
    
    if let birthdate = birthDateTextField.text {
      self.birthDate = birthdate
    }
    
    if let gender = genderButton.title(for: .normal) {
      self.gender = gender
    }
    
    
    
    
    presenter.updateProfile(name: name, email: email, phone: phone, birthDate: birthDate, gender: gender,cityID: cityID, countryID: countryID, profileImage: profileImage, coverImage: coverImage)
  }
  
  @IBAction func profileImageTapped(_ sender: UITapGestureRecognizer) {
    IsCoverPhoto = false
    self.photoSourceActionSheet()
  }
  
  @IBAction func coverImageTapped(_ sender: Any) {
    IsCoverPhoto = true
    self.photoSourceActionSheet()
  }
  
  func showDatePicker(){
    //Formate Date
    datePicker.datePickerMode = .date
    
    //ToolBar
    let toolbar = UIToolbar();
    toolbar.sizeToFit()
    let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
    let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
    
    toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
    
    birthDateTextField.inputAccessoryView = toolbar
    birthDateTextField.inputView = datePicker
    
  }
  
  @objc func donedatePicker(){
    
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    birthDateTextField.text = formatter.string(from: datePicker.date)
    self.view.endEditing(true)
  }
  
  @objc func cancelDatePicker(){
    self.view.endEditing(true)
  }

  
}

extension ProfileVC: ProfileViewDelegate {
  func segueToHome() {
    self.segueToMainTabBar()
  }
  
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()
  }
  
  func displayError() {
    
  }
  
  func displayError(error: String) {
    self.showToast(message: error)
  }
  
  func displayUserData(user: SingleUserDataModel) {
//    self.showToast(message: "welcome back \(user.name!)")

    
    nameTextField.text = user.name
    emailTextField.text = user.email
    phoneTextField.text = user.phone
    birthDateTextField.text = user.birthDate
    cityButton.setTitle(user.city, for: .normal)
    countryButton.setTitle(user.country, for: .normal)
    genderButton.setTitle(user.gender, for: .normal)
    
    if let cityId = user.cityID {
      self.cityID = cityId
    }
    
    if let countyId = user.countryID {
      self.countryID = countyId
    }
    
    if !isCoverPhotoEdited {
      coverImageView.setImage(url: user.cover)
    }
    if !isProfilePhotoEdited {
      profileImageView.setImage(url: user.profilePricture)
    }
    isCoverPhotoEdited = false
    isProfilePhotoEdited = false
  }
 
}


extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    imagePicker.dismiss(animated: true, completion: nil)
  }
  
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    
    if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
      if IsCoverPhoto {
        self.coverImage = editedImage
      } else {
        self.profileImage = editedImage
      }
      
    } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
      if IsCoverPhoto {
        self.coverImage = originalImage
      } else {
        self.profileImage = originalImage
      }
    }
    imagePicker.dismiss(animated: true, completion: nil)
    
  }
  
}
