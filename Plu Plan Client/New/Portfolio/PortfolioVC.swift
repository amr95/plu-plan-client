//
//  PortfolioVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/11/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import Segmentio
import Kingfisher

    class PortfolioVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var curvedView: UIView!
    @IBOutlet weak var buttonTabs: Segmentio!
    @IBOutlet weak var collectionView: UICollectionView!
        
    var selectedIndex = 0 { didSet { collectionView.reloadData()}}
    let presenter = PortfolioPresenter()
      
      var albums = [AlbumDetails]() {
        didSet {
          collectionView.reloadData()
        }
      }
      
      var packages = [PackageData]()
//      {
//        didSet {
//          collectionView.reloadData()
//        }
//      }
      
      
      
    var content = [SegmentioItem]()
    var albumID = 0
    let tornadoItem = SegmentioItem(
        title: "Portfolio",
        image: nil)
    
    let tornadoItem2 = SegmentioItem(
        title: "Reviews",
        image: nil)
    
    let tornadoItem3 = SegmentioItem(
        title: "Packges",
        image: nil)
        
      let tabsState  = SegmentioStates(
        defaultState: SegmentioState(
          backgroundColor: .clear,
          titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
          titleTextColor: .black
        ),
        selectedState: SegmentioState(
          backgroundColor: .white,
          titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
          titleTextColor: #colorLiteral(red: 1, green: 0.4117647059, blue: 0.4117647059, alpha: 1)
        ),
        highlightedState: SegmentioState(
          backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
          titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
          titleTextColor: .black
        )
      )
        
      let tabsOptions = SegmentioOptions(backgroundColor: .clear, segmentPosition: SegmentioPosition.dynamic,
                                         scrollEnabled: true, indicatorOptions: SegmentioIndicatorOptions(type: .bottom, ratio: 1, height: 3, color: #colorLiteral(red: 1, green: 0.4117647059, blue: 0.4117647059, alpha: 1)), horizontalSeparatorOptions: SegmentioHorizontalSeparatorOptions(
                                          type: SegmentioHorizontalSeparatorType.bottom,
                                          height: 0,
                                          color: .white
        ),
                                         verticalSeparatorOptions: SegmentioVerticalSeparatorOptions(ratio: 1, color: .clear), imageContentMode: .center, labelTextAlignment: .center, labelTextNumberOfLines: 1,
                                         segmentStates: SegmentioStates(
                                          defaultState: SegmentioState(
                                            backgroundColor: .clear,
                                            titleFont: AppFont.getFont(type: .normalRegular, size: 12),
                                            titleTextColor: .lightGray
                                          ),
                                          selectedState: SegmentioState(
                                            backgroundColor: .white,
                                            titleFont: AppFont.getFont(type: .noramlBold, size: 14),
                                            titleTextColor: #colorLiteral(red: 1, green: 0.4117647059, blue: 0.4117647059, alpha: 1)
                                          ),
                                          highlightedState: SegmentioState(
                                            backgroundColor: UIColor.lightGray.withAlphaComponent(0.6),
                                            titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
                                            titleTextColor: .black
                                          )
      ))

    

      
    override func viewDidLoad() {
        super.viewDidLoad()
      print("jwt = \(UserDefaults.standard.string(forKey: "jwt")!)")
        presenter.attatchView(view: self)
        presenter.startNetworking()
        presenter.fetchPackagesData()

      
        collectionView.dataSource = self
        collectionView.delegate = self
      
      // MARK: - Register Cell
        collectionView.registerCellNib(cellClass: AlbumCVC.self)
        collectionView.registerCellNib(cellClass: AddNewAlbumCVC.self)

        collectionView.registerCellNib(cellClass: ReviewsCVC.self)
      
        collectionView.registerCellNib(cellClass: PackagesCVC.self)
        collectionView.registerCellNib(cellClass: AddNewPackageCVC.self)
        
        content.append(tornadoItem)
        content.append(tornadoItem2)
        content.append(tornadoItem3)


        // Do any additional setup after loading the view.
        buttonTabs.setup(
            content: content,
            style: SegmentioStyle.onlyLabel,
            options: tabsOptions
        )
        buttonTabs.selectedSegmentioIndex = self.selectedIndex
      
      // MARK: - add long press to collectionview
      let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(longPressGestureRecognizer:)))
      collectionView.addGestureRecognizer(longPress)
    }
      
      override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.startNetworking()
        presenter.fetchPackagesData()
        collectionView.reloadData()
      }
      
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
            self.collectionView.reloadData()

            buttonTabs.valueDidChange = { segmentio, segmentIndex in
                print("Selected item: ", segmentIndex)
                self.selectedIndex = segmentIndex
              if segmentIndex == 2 {
                self.presenter.fetchPackagesData()

              } else if segmentIndex == 0 {
                self.presenter.startNetworking()
              }
            }
        }

    override func viewDidLayoutSubviews() {
        profileImage.layer.cornerRadius = 60
        curvedView.layer.cornerRadius = 80
        
    }
      
      // MARK: - Handle long press on collectionview cell
      @objc func handleLongPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
          let touchPoint = longPressGestureRecognizer.location(in: self.collectionView)
          if let indexPath = self.collectionView.indexPathForItem(at: touchPoint) {
            // add your code here
            // you can use 'indexPath' to find out which row is selected
            if selectedIndex == 0 {
              if (indexPath.row - 1) >= 0 {
                if let albumID = albums[indexPath.row - 1].id {
                  self.albumID = albumID
                  self.showLongpressAlert(indexPath: indexPath)
                }
              }

            } else if selectedIndex == 2 {
              if (indexPath.row - 1) >= 0 {
                if let packageID = packages[indexPath.row - 1].id {
                  self.showLongpressAlert(indexPath: indexPath)
                  print("package id = \(packageID)")
                }
              }
            }
          }
        }
      }
      
      
      // MARK: - Delete Alert
      fileprivate func deleteAlert(title: String, message: String, indexPath: IndexPath) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let yesDelete = UIAlertAction(title: "Yes sure", style: .destructive) { (action: UIAlertAction) in
          if self.selectedIndex == 0 {
            self.presenter.deleteAlbum(albumID: self.albumID)
          } else if self.selectedIndex == 2 {
            print("hello from delete")
            self.presenter.deletePackage(packageID: self.packages[indexPath.row-1].id!)
          }
          
        }
        
        let noThanks = UIAlertAction(title: "No Thanks", style: .cancel) { (action: UIAlertAction) in
        }

        alert.addAction(yesDelete)
        alert.addAction(noThanks)
        
        self.presentVC(alert)
      }

      fileprivate func showLongpressAlert(indexPath: IndexPath) {
        let alert = UIAlertController(title: "", message: "What do you want ?", preferredStyle: .actionSheet)
        let editAction = UIAlertAction(title: "Edit", style: .default) { (action: UIAlertAction) in
          if self.selectedIndex == 0 {
            let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
            let nav = storyboard.instantiateViewController(withIdentifier: "ViewAlbum") as! ViewAlbumImagesVC
            guard let albumID = self.albums[indexPath.row - 1].id else {return}
            guard let albumName = self.albums[indexPath.row - 1].name else {return}
            nav.albumID = albumID
            nav.albumName = albumName
            print(albumID)
            UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
          
          
          
          } else if self.selectedIndex == 2 {
            let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
            let nav = storyboard.instantiateViewController(withIdentifier: "AddNewPackage") as! AddNewPackageVC
            nav.buttonTittle = "update package"
            nav.name = self.packages[indexPath.row - 1].title!
            nav.desc = self.packages[indexPath.row - 1].datumDescription!
            nav.price = self.packages[indexPath.row - 1].price!
            nav.imageUrl = self.packages[indexPath.row-1].image!
            nav.packageID = self.packages[indexPath.row-1].id!
            UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)

          }
        }
        
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (action: UIAlertAction) in
          
          if self.selectedIndex == 0 {
            self.deleteAlert(title: "Delete Album", message: "Are you sure you want to delete this Album !!", indexPath: indexPath)
          } else if self.selectedIndex == 2 {
            self.deleteAlert(title: "Delete Package", message: "Are you sure you want to delete this Package !!", indexPath: indexPath)
          }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action: UIAlertAction) in
        }
        
        alert.addAction(editAction)
        alert.addAction(deleteAction)
        alert.addAction(cancelAction)
        
        self.presentVC(alert)
      }
      
}



extension PortfolioVC: PortfolioViewDelegate {
  func displayData(with packages: [PackageData]) {
    self.packages = packages
  }
  
  func displayLoading() {
    self.view.showActivityIndicator()
  }
  
  func dismissLoading() {
    self.view.hideActivityIndicator()

  }
  
  func refesh() {
    self.presenter.startNetworking()
    self.collectionView.reloadData()
  }
  
  func displayError(error: String) {
    
  }
  
  func displayAlbumData(with album: [AlbumDetails]) {
    self.albums = album
  }
  
  
}
