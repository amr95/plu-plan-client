//
//  PortfolioPresenter.swift
//  We Plan
//
//  Created by apple on 11/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation

protocol PortfolioViewDelegate {
  func displayLoading()
  func dismissLoading()
  func refesh()
  func displayError(error: String)
  func displayAlbumData(with album: [AlbumDetails])
  func displayData(with packages: [PackageData])

}

class PortfolioPresenter {
  
  var delegate: PortfolioViewDelegate?
  
  // attatch view to presenter
  func attatchView(view: PortfolioViewDelegate){
    delegate = view
  }
  // detach view to presenter
  func detachView(){
    delegate = nil
  }
  
  func startNetworking() {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest(getAlbumsModel.self, router: .getAlbums, success: { (albumsData) in
      self.delegate?.dismissLoading()
      print("Album Data = \(albumsData)")
      self.processData(with: albumsData as AnyObject)
    }) { (error) in
      self.delegate?.dismissLoading()

    }
}
  
  func fetchPackagesData () {
    self.delegate?.displayLoading()
    
    NetworkClient.performRequest2(SinglePackage.self, router: .getPackages, model: nil, success: { (response) in
      self.delegate?.dismissLoading()
      
      if let data = response.data {
      self.delegate?.displayData(with: data)
      }
      
    }) { (error) in
      self.delegate?.dismissLoading()
      self.delegate?.displayError(error: error)
    }
    
  }
  
  
  func processData(with Model: AnyObject) {
    let albums = Model as! getAlbumsModel
    if let albumsArray = albums.data {
      self.sendDataToView(with: albumsArray)
    } else {
      return
    }
    
  }
  
  //checking the data count and if ispaginating or not to send it to the view
  func sendDataToView(with data : [AlbumDetails]) {
    
//    DispatchQueue.main.async {
//      if data.count == 0 {
//        if !self.isPaginating {
//          self.movieView?.displayError()
//        }
//        return
//      }
//      if !self.isPaginating {
//        self.movieView?.displayData(with: data)
//      }
//      else {
//        self.movieView?.displayPagintationData(with: data)
//      }
//    }
    self.delegate?.displayAlbumData(with: data)
    
  }
  
  func deleteAlbum(albumID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deleteAlbum(albumId: albumID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.refesh()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
  
  func deletePackage(packageID: Int) {
    delegate?.displayLoading()
    
    NetworkClient.performRequest2(AlbumImages.self, router: .deletePackage(packageId: packageID), model: nil , success: { (result) in
      self.delegate?.dismissLoading()
      self.delegate?.refesh()
      print(result.data)
    }) { (error) in
      self.delegate?.dismissLoading()
      print(error)
    }
    
  }
}
