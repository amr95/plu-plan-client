//
//  Portfolio+CollectionDelegates.swift
//  We Plan
//
//  Created by apple on 11/21/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import Foundation
import UIKit


extension PortfolioVC: UICollectionViewDelegate, UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    print("Count = \(albums.count)")
    if self.selectedIndex == 0 {
      return self.albums.count + 1
      
    } else if self.selectedIndex == 2 {
      print("Count pac = \(packages.count)")
      
      return self.packages.count + 1
    } else {
      return 10
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    // Portfolio Cell
    if selectedIndex == 0 {
      if indexPath.row == 0 {
        let cell = collectionView.dequeue(indexPath: indexPath) as AddNewAlbumCVC
        cell.plusIV.layer.borderWidth = 2
        cell.plusIV.layer.borderColor = #colorLiteral(red: 0.8374180198, green: 0.8374378085, blue: 0.8374271393, alpha: 1)
        return cell
      } else {
        let cell = collectionView.dequeue(indexPath: indexPath) as AlbumCVC
        
        if self.albums.count > 0 {
          if let imageURL = self.albums[indexPath.row - 1].img {
            if !imageURL.isEmpty {
              cell.albumImage.kf.indicatorType = .activity
              cell.albumImage.kf.setImage(with: URL(string: imageURL) )
              
            } else {
              cell.albumImage.image = UIImage(named: "x1")
            }
          }
        }
        return cell
      }
    }
      
    else if selectedIndex == 1 {
      let cell = collectionView.dequeue(indexPath: indexPath) as ReviewsCVC
      return cell
    } else {
      // MARK: - packages
      
      
      if indexPath.row == 0 {
        let cell = collectionView.dequeue(indexPath: indexPath) as AddNewPackageCVC

        return cell
      } else {
        let cell = collectionView.dequeue(indexPath: indexPath) as PackagesCVC
        cell.configureCell(packagesData: packages[indexPath.row - 1])
        return cell
      }
      
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    // MARK: - Protfolio Item DidSelected
    if selectedIndex == 0 {
      if indexPath.row == 0 {
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "AddNewAlbum")
        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
      } else {
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "ViewAlbum") as! ViewAlbumImagesVC
        guard let albumID = albums[indexPath.row - 1].id else {return}
        guard let albumName = albums[indexPath.row - 1].name else {return}
        nav.albumID = albumID
        nav.albumName = albumName
        print(albumID)
        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
      }
    }
      
    else if selectedIndex == 2 {
      if indexPath.row == 0 {
        let storyboard = UIStoryboard.init(name: "Profile", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "AddNewPackage")
        UIApplication.shared.keyWindow?.visibleViewController?.present(nav, animated: true, completion: nil)
      } else {
        
      }
    }
    
    
  }
  
}



extension PortfolioVC : UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    if selectedIndex == 0 {
      return CGSize(width: 150, height: 150)
    } else if selectedIndex == 1 {
      return CGSize(width: UIScreen.main.bounds.width, height: 130.0)
    } else {
      return CGSize(width: UIScreen.main.bounds.width, height: 110.0)
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    if selectedIndex == 0 {
      return UIEdgeInsets(top: 15.0, left: 30.0, bottom: 0.0, right: 30.0)
    } else {
      return UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    }
  }
  
}

enum TabsType {
  case albums
  case reviews
  case packages
}
