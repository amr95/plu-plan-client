//
//  SelectTittleVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/10/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class SelectTittleVC: UIViewController {

  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var makeupButton: UIButton!
  @IBOutlet weak var photoButton: UIButton!
  
  override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view
    }
    
    override func viewDidLayoutSubviews() {
        nextButton.setGradientBackground(colorOne: #colorLiteral(red: 1, green: 0.6, blue: 0.4, alpha: 1), colorTwo: #colorLiteral(red: 1, green: 0.4, blue: 0.6, alpha: 1))
    }
  
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: false)
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    self.navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
  func selectedButton(button: UIButton) {
    button.layer.shadowColor = #colorLiteral(red: 0.9315226674, green: 0.2321678102, blue: 0.3410214186, alpha: 1)
    button.layer.shadowOpacity = 0.7
    button.layer.shadowOffset = CGSize(width: 0, height: 0)
  }

  func deSelectedButton(button: UIButton) {
    button.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    button.layer.shadowOpacity = 0.7
    button.layer.shadowOffset = CGSize(width: 0, height: 0)
  }
  
  
  @IBAction func didTappedOnTittle(_ sender: UIButton) {
    if sender.tag == 11 {
      
      Constants.category = "1"

        deSelectedButton(button: makeupButton)
        selectedButton(button: photoButton)
      
    } else if sender.tag == 22 {
      
      Constants.category = "2"
      deSelectedButton(button: photoButton)
      selectedButton(button: makeupButton)
    }
    
    print("tittle = \(Constants.category)")
  }
  
}
