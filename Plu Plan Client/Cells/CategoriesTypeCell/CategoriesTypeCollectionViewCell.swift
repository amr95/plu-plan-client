//
//  CategoriesTypeCollectionViewCell.swift
//  Plu Plan Client
//
//  Created by apple on 2/25/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//

import UIKit

class CategoriesTypeCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var categoryImageView: UIImageView!
  
  @IBOutlet weak var categoryNameLabel: UILabel!
  @IBOutlet weak var cellView: UIView!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
