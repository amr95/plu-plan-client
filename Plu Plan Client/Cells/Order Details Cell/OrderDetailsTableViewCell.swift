//
//  OrderDetailsTableViewCell.swift
//  Plu Plan Client
//
//  Created by apple on 2/27/20.
//  Copyright © 2020 Amr Saleh. All rights reserved.
//

import UIKit
import Cosmos
import StepIndicator

class OrderDetailsTableViewCell: UITableViewCell {

  @IBOutlet weak var providerNameLabel: UILabel!
  @IBOutlet weak var providerRating: CosmosView!
  @IBOutlet weak var providerImageView: UIImageView!
  
  @IBOutlet weak var orderDateLabel: UITextField!
  @IBOutlet weak var packageNameLabel: UITextField!
  @IBOutlet weak var priceLabel: UITextField!
  @IBOutlet weak var descriptionLabel: UITextField!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var addressDetailsLabel: UITextField!
  @IBOutlet weak var phoneLabel: UITextField!
  @IBOutlet weak var orderImageView: UIImageView!
  
  @IBOutlet weak var stepIndicatorView: StepIndicatorView!
  
  @IBOutlet weak var pendingLabel: UILabel!
  @IBOutlet weak var acceptedLabel: UILabel!
  @IBOutlet weak var finishedLabel: UILabel!
  
  @IBOutlet weak var cancelOrderButton: UIButton!
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configurCell(order: SingleOrderDetails) {
    providerNameLabel.text = order.customerName
    providerRating.rating = Double(order.rate!)
    providerImageView.setImage(url: order.customerProfilePricture)
    
    orderDateLabel.text = order.date
    packageNameLabel.text = order.title
    priceLabel.text = order.price! + " EGP"
    descriptionLabel.text = order.orderDescription
    addressLabel.text = order.location
    addressDetailsLabel.text = order.addressDetails
    phoneLabel.text = order.phone
    
    orderImageView.setImage(url: order.packagePicture)
    
    if Int(order.status!)! == 0 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 1
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.black
      finishedLabel.textColor = UIColor.gray
    } else if Int(order.status!)! == 1 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 2
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
      finishedLabel.textColor = UIColor.gray
    } else if Int(order.status!)! == 2 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 3
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
      finishedLabel.textColor = UIColor.WePaln.primaryColor
      cancelOrderButton.isHidden = true
    } else if Int(order.status!)! == 3 {
      stepIndicatorView.numberOfSteps = 1
      stepIndicatorView.currentStep = 2
      pendingLabel.isHidden = true
      finishedLabel.isHidden = true
      acceptedLabel.text = "canceled"
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
      cancelOrderButton.isHidden = true
    }
  }
  
  
  @IBAction func providerButtonTapped(_ sender: Any) {
  }
  
  @IBAction func cancleButtonTapped(_ sender: Any) {
  }
}
