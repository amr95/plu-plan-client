//
//  ImagesCVC.swift
//  We Plan
//
//  Created by apple on 11/17/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

protocol LongPressDelegate {
  func didPressedLong(imageID: Int)
  func didClickOnImage(image: UIImage, imageID: Int)
}

class ImagesCVC: UICollectionViewCell {
  @IBOutlet weak var albumImage: UIImageView!
  
    var delegate: LongPressDelegate!
    var imageID = 0
  
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(sender:)))
      
        self.albumImage.addGestureRecognizer(longPress)
    }

  
  @objc func handleLongPress(sender: UILongPressGestureRecognizer) {
    if sender.state == .began {
      self.delegate.didPressedLong(imageID: self.imageID)
    }
  }
  
  // MARK: - Methods
  
  /**
   Configure views with it's data according to API response.
   - Parameter imageUrl : url for image to display.
   */
  func configureCell(imageData:ImagesData) {
    if let id = imageData.id {
      self.imageID = id
      if let imageURL = imageData.ur {
          albumImage.setImage(url: imageURL)
      }
    }
    
    self.addTapGesture { [weak self] (_) in
      self?.delegate?.didClickOnImage(image: (self?.albumImage.image!)!, imageID: (self?.imageID)!)
    }
  }
  
}
