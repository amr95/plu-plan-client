//
//  PackagesCVC.swift
//  We Plan
//
//  Created by Amr Saleh on 10/30/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

class PackagesCVC: UICollectionViewCell {

    @IBOutlet weak var packagePrice: UILabel!
    @IBOutlet weak var packageDescriptionLabel: UILabel!
    @IBOutlet weak var packageImage: UIImageView!
    @IBOutlet weak var packageTitle: UILabel!
  
  
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  
  func configureCell(packagesData:PackageData) {
    if let imageURL = packagesData.image {
      packageImage.setImage(url: imageURL)
    }
    self.packageTitle.text = packagesData.title
    self.packageDescriptionLabel.text = packagesData.datumDescription
    self.packagePrice.text = packagesData.price ?? "ERROR" + "EGP"
  }
  
}
