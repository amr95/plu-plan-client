//
//  OrdersCell.swift
//  We Plan
//
//  Created by apple on 12/29/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit
import StepIndicator

protocol OrdersCellDelegate {
  func viewOrderDetails(orderDetails: SingleOrderDetails)
}
class OrdersCell: UITableViewCell {

  @IBOutlet weak var packageNameLabel: UILabel!
  @IBOutlet weak var orderDateLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var providerNameLabel: UILabel!
  @IBOutlet weak var stepIndicatorView: StepIndicatorView!
  @IBOutlet weak var pendingLabel: UILabel!
  @IBOutlet weak var acceptedLabel: UILabel!
  @IBOutlet weak var finishedLabel: UILabel!
  
  var orderDetails: SingleOrderDetails!
  var delegate: OrdersCellDelegate?
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  @IBAction func viewDetailsButton(_ sender: Any) {
    self.delegate?.viewOrderDetails(orderDetails: self.orderDetails)
  }
  
  func configurCell(order: SingleOrderDetails) {
    packageNameLabel.text = order.title
    orderDateLabel.text = order.date
    priceLabel.text = order.price! + " EGP"
    providerNameLabel.text = order.customerName
    orderDetails = order
    
    if Int(order.status!)! == 0 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 1
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.black
      finishedLabel.textColor = UIColor.gray
    } else if Int(order.status!)! == 1 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 2
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
      finishedLabel.textColor = UIColor.gray
    } else if Int(order.status!)! == 2 {
      stepIndicatorView.numberOfSteps = 3
      stepIndicatorView.currentStep = 3
      pendingLabel.textColor = UIColor.WePaln.primaryColor
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
      finishedLabel.textColor = UIColor.WePaln.primaryColor
    } else if Int(order.status!)! == 3 {
      stepIndicatorView.numberOfSteps = 1
      stepIndicatorView.currentStep = 2
      pendingLabel.isHidden = true
      finishedLabel.isHidden = true
      acceptedLabel.text = "canceled"
      acceptedLabel.textColor = UIColor.WePaln.primaryColor
    }
  }
}
