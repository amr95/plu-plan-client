//
//  AddCVC.swift
//  We Plan
//
//  Created by apple on 11/19/19.
//  Copyright © 2019 Amr Saleh. All rights reserved.
//

import UIKit

protocol addDidTappedDelegate {
  func addImageDidTapped()
}

class AddCVC: UICollectionViewCell {

  @IBOutlet weak var addImageView: UIImageView!
  var delegate: addDidTappedDelegate!

  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleImageTapped(sender:)))
    self.addImageView.addGestureRecognizer(tapGesture)

    }
  
  @objc func handleImageTapped(sender: UITapGestureRecognizer) {
    
    self.delegate.addImageDidTapped()
    
  }

}
